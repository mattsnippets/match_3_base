﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Object pool class.
/// </summary>
public class ObjectPool : MonoBehaviour
{
    [SerializeField]
    private GameObject pooledType = null;
    [SerializeField]
    private int startCount = 0;
    [SerializeField]
    private bool shouldGrow = true;
    [SerializeField]
    private int objectCount;

    private List<GameObject> pooledObjects = new List<GameObject>();

    public GameObject PooledType
    {
        get
        {
            return pooledType;
        }

        set
        {
            pooledType = value;
        }
    }

    /// <summary>
    /// Initialize the pool with start count objects.
    /// </summary>
    private void Start()
    {
        for (int i = 0; i < startCount; i++)
        {
            AddObject();
        }

        objectCount = startCount;
    }

    /// <summary>
    /// Return the first inactive object from the pool. If every object is active and shouldGrow is
    /// true, add a new object and return its reference. Otherwise return null. If test mode is on, 
    /// return a non-pooled object instance (skip pooling functionality).
    /// </summary>
    /// <param name="isTestModeOn">If true, a non-pooled object is returned</param>
    /// <returns>A previously unused GameObject (or null if the pool reached its limit and cannot
    /// grow anymore). If test mode is used, a non-pooled instance is returned.</returns>
    public GameObject GetPooledObject(bool isTestModeOn)
    {
        if(!isTestModeOn)
        {
            for (int i = 0; i < pooledObjects.Count; i++)
            {
                if (!pooledObjects[i].activeInHierarchy)
                {
                    return pooledObjects[i];
                }
            }

            if (shouldGrow)
            {
                AddObject();
                return pooledObjects.Last();
            }

            return null;
        }
        else
        {
            return Instantiate(pooledType);
        }
    }

    /// <summary>
    /// Instantiate and add a GameObject of the pooled type to pool.
    /// </summary>
    public void AddObject()
    {
        GameObject obj = Instantiate(pooledType);
        obj.SetActive(false);
        pooledObjects.Add(obj);
        objectCount++;
    }
}
