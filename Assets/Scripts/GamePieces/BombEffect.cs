﻿using UnityEngine;

public class BombEffect : MonoBehaviour
{
    [SerializeField]
    private GameObject bombSparklePrefab;
    [SerializeField]
    private GameObject bombSparkleBlockPrefab;
    [SerializeField]
    private float effectSpeed = 12f;

    private int x;
    private int y;

    public void Init()
    {
        switch (GetComponent<Bomb>().BombType)
        {
            case BombType.Block:
                SpawnBlockEffect();
                break;
            case BombType.Column:
                SpawnEffect(BombType.Column, transform.position, 1f);
                SpawnEffect(BombType.Column, transform.position, -1f);
                break;
            case BombType.Row:
                SpawnEffect(BombType.Row, transform.position, 1f);
                SpawnEffect(BombType.Row, transform.position, -1f);
                break;
            case BombType.Color:
                break;
        }
    }

    private void SpawnEffect(BombType bombType, Vector3 pos, float velMultiplier)
    {
        GameObject bombSparkleGo = ObjectPoolManager.Current.GetPooledObject(bombType == BombType.Block ? bombSparkleBlockPrefab : bombSparklePrefab);

        if (bombType == BombType.Block)
        {
            bombSparkleGo.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, Random.Range(0f,360f)));
        }
        else
        {
            bombSparkleGo.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, bombType == BombType.Row ? 90f : 0f));
        }

        bombSparkleGo.transform.position = pos + new Vector3(0f,0f,-5f);
        bombSparkleGo.SetActive(true);

        foreach (Transform child in bombSparkleGo.transform)
        {
            child.gameObject.SetActive(true);
        }

        bombSparkleGo.GetComponent<Rigidbody2D>().velocity = bombSparkleGo.transform.up * effectSpeed * velMultiplier;
    }

    private void SpawnBlockEffect()
    {
        x = Mathf.RoundToInt(transform.parent.position.x);
        y = Mathf.RoundToInt(transform.parent.position.y);

        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {
                if ((x + i >= 0 && x + i < 8)
                    && (y + j >= 0 && y + j < 8))
                {
                    SpawnEffect(BombType.Block, transform.position + new Vector3(i, j, 0f), 0f);
                }
            }
        }
    }
}
