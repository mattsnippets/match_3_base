﻿using UnityEngine;
using UnityEngine.Assertions;

/// <summary>
/// Board tile class.
/// </summary>
public class Tile : MonoBehaviour
{
    [SerializeField]
    private TileType type;

    private int x;
    private int y;
    private InputHandler inputHandler;

    public TileType Type
    {
        get
        {
            return type;
        }
    }

    public int X
    {
        get
        {
            return x;
        }
    }

    public int Y
    {
        get
        {
            return y;
        }
    }

    /// <summary>
    /// Initialize tile with coordinates and an InputHandler reference
    /// </summary>
    /// <param name="x">X coordinate</param>
    /// <param name="y">Y coordinate</param>
    /// <param name="inputHandler">InputHandler reference</param>
    public void Init(int x, int y, InputHandler inputHandler)
    {
        this.x = x;
        this.y = y;
        this.inputHandler = inputHandler;

        Assert.IsNotNull(inputHandler);
    }

    void OnMouseDown()
    {
        CustomLogger.Log(string.Format("OnMouseDown: {0} {1}", X, Y), LogPriority.Info);
        inputHandler.ClickTile(this);
    }

    void OnMouseEnter()
    {
        CustomLogger.Log(string.Format("OnMouseEnter: {0} {1}", X, Y), LogPriority.Info);
        inputHandler.DragTile(this);
    }

    void OnMouseUp()
    {
        CustomLogger.Log(string.Format("OnMouseUp: {0} {1}", X, Y), LogPriority.Info);
        inputHandler.ReleaseTile();
    }
}

public enum TileType
{
    Basic,
    Obstacle
}