﻿using UnityEngine;

/// <summary>
/// Bomb component class.
/// </summary>
public class Bomb : MonoBehaviour
{
    [SerializeField]
    private BombType bombType;
    [SerializeField]
    private ParticleSystem bombSparkleColor;

    private int scoreValue;

    /// <summary>
    /// Initialize bomb, set type, score value and sprite based on the bomb type.
    /// </summary>
    /// <param name="type">Type of bomb</param>
    /// <param name="scoreValue">Score value of bomb</param>
    public void Init(BombType type, int scoreValue)
    {
        bombType = type;
        this.scoreValue = scoreValue;

        if (type == BombType.Color)
        {
            Instantiate(bombSparkleColor, transform.position, Quaternion.identity, transform);
        }

        SetupAnimation();
    }

    public BombType BombType
    {
        get
        {
            return bombType;
        }
    }

    public int ScoreValue
    {
        get
        {
            return scoreValue;
        }
    }

    private void OnValidate()
    {
        //SetupAnimation();
    }

    private void SetupAnimation()
    {
        Transform animationObject = transform.Find(bombType.ToString());

        if (animationObject != null)
        {
            animationObject.gameObject.SetActive(true);
        }
    }
}

public enum BombType
{
    Block,
    Color,
    Column,
    Row
}

/// <summary>
/// Data object used in instantiating new bombs during gameplay.
/// </summary>
public class BombData
{
    public int x;
    public int y;
    public BombType bombType;
    public PieceType createdPieceType;

    public BombData(int x, int y, BombType bombType, PieceType createdPieceType)
    {
        this.x = x;
        this.y = y;
        this.bombType = bombType;
        this.createdPieceType = createdPieceType;
    }
}
