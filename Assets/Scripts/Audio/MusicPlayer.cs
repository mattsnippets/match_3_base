﻿using Mattsnippets.EventSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicPlayer : MonoBehaviour
{
    private readonly Dictionary<string, AudioClip> singleTracks = new Dictionary<string, AudioClip>();
    private List<AudioClip> randomTracks;

    public static MusicPlayer Instance;

    private AudioSource audioSource;
    private int randomTrackIndex = -1;
    private bool isRandomEnabled;
    private float fadeOutSpeed = 2f;
    private Coroutine fadeOutCoroutine;
    private string track;
    private string sceneName;

    public void PlayRandom()
    {
        if (fadeOutCoroutine == null)
        {
            SetupRandomPlay();
        }
        else
        {
            StartCoroutine(DelayedCall(SetupRandomPlay));
        }
    }

    private void SetupRandomPlay()
    {
        audioSource.volume = 1f;
        isRandomEnabled = true;
        audioSource.loop = false;
        audioSource.clip = GetRandomTrack();
        audioSource.Play();
    }

    public void PlaySingle(string trackName)
    {
        track = trackName;

        if (fadeOutCoroutine == null)
        {
            SetupSinglePlay();
        }
        else
        {
            StartCoroutine(DelayedCall(SetupSinglePlay));
        }
    }

    private void SetupSinglePlay()
    {
        audioSource.volume = 1f;
        isRandomEnabled = false;
        audioSource.loop = true;
        audioSource.clip = singleTracks[track];
        audioSource.Play();
    }

    public void Stop()
    {
        audioSource.Stop();
    }

    public void FadeOutStop()
    {
        if (fadeOutCoroutine == null)
        {
            fadeOutCoroutine = Instance.StartCoroutine(FadeOut());
        }
    }

    private void OnEnable()
    {
        Instance.PlaySingle("Menu");
        StartCoroutine(CheckIfTrackIsPlaying());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;

            MusicEnabled.AddListener(OnMusicEnabled);

            sceneName = SceneManager.GetActiveScene().name;

            audioSource = GetComponent<AudioSource>();

            AudioClip[] trackAssets = Resources.LoadAll<AudioClip>("Audio/Music/Random");
            randomTracks = new List<AudioClip>(trackAssets);

            trackAssets = Resources.LoadAll<AudioClip>("Audio/Music/Single");

            foreach (AudioClip track in trackAssets)
            {
                singleTracks[track.name] = track;
            }

            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        gameObject.SetActive(Convert.ToBoolean(PlayerPrefs.GetInt("MusicEnabled", 1)));
    }

    private void OnMusicEnabled(MusicEnabled eventData)
    {
        gameObject.SetActive(eventData.isEnabled);
    }

    private void OnSceneLoaded(Scene newScene, LoadSceneMode loadSceneMode)
    {
        if (gameObject.activeInHierarchy && loadSceneMode != LoadSceneMode.Additive && newScene.name != sceneName)
        {
            FadeOutStop();

            switch (newScene.name)
            {
                case "LevelSelect":
                    Instance.PlaySingle("Menu");
                    break;
                default:
                    Instance.PlayRandom();
                    break;
            }

            sceneName = newScene.name;
        }
    }

    private AudioClip GetRandomTrack()
    {
        int index;

        do
        {
            index = UnityEngine.Random.Range(0, randomTracks.Count);
        } while (index == randomTrackIndex);

        randomTrackIndex = index;
        return randomTracks[index];
    }

    private IEnumerator CheckIfTrackIsPlaying()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);

            if (isRandomEnabled && !audioSource.isPlaying)
            {
                audioSource.clip = GetRandomTrack();
                audioSource.Play();
            }
        }
    }

    private IEnumerator FadeOut()
    {
        while (audioSource.volume > 0f)
        {
            audioSource.volume -= Time.unscaledDeltaTime * fadeOutSpeed;
            yield return null;
        }

        audioSource.Stop();
        fadeOutCoroutine = null;
    }

    private IEnumerator DelayedCall(Action delayedAction)
    {
        yield return new WaitForSecondsRealtime(0.5f);
        delayedAction();
    }
}
