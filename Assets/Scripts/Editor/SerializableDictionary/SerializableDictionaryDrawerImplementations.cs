﻿using UnityEngine;

// ---------------
//  String => Int
// ---------------
[UnityEditor.CustomPropertyDrawer(typeof(StringIntDictionary))]
public class StringIntDictionaryDrawer : SerializableDictionaryDrawer<string, int>
{
    protected override SerializableKeyValueTemplate<string, int> GetTemplate()
    {
        return GetGenericTemplate<SerializableStringIntTemplate>();
    }
}
internal class SerializableStringIntTemplate : SerializableKeyValueTemplate<string, int> { }

// ---------------
//  GameObject => Float
// ---------------
[UnityEditor.CustomPropertyDrawer(typeof(GameObjectFloatDictionary))]
public class GameObjectFloatDictionaryDrawer : SerializableDictionaryDrawer<GameObject, float>
{
    protected override SerializableKeyValueTemplate<GameObject, float> GetTemplate()
    {
        return GetGenericTemplate<SerializableGameObjectFloatTemplate>();
    }
}
internal class SerializableGameObjectFloatTemplate : SerializableKeyValueTemplate<GameObject, float> { }

// ---------------
//  String => Color
// ---------------
[UnityEditor.CustomPropertyDrawer(typeof(StringColorDictionary))]
public class StringColorDictionaryDrawer : SerializableDictionaryDrawer<string, Color>
{
    protected override SerializableKeyValueTemplate<string, Color> GetTemplate()
    {
        return GetGenericTemplate<SerializableStringColorTemplate>();
    }
}
internal class SerializableStringColorTemplate : SerializableKeyValueTemplate<string, Color> { }