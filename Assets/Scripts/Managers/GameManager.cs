﻿using Mattsnippets.EventSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Game manager controlling game flow, score calculation, game over conditions, GUI updates.
/// </summary>
public class GameManager : MonoBehaviour
{
    [SerializeField]
    private bool isScoreLimitSet = false;
    [SerializeField]
    private bool isMoveLimitSet = false;
    [SerializeField]
    private bool isTimeLimitSet = false;
    [SerializeField]
    private int score;
    [SerializeField]
    private int scoreLimit;
    [SerializeField]
    private float remainingTime;
    [SerializeField]
    private GuiManager guiManager;
    [SerializeField]
    private int remainingMoves;

    private bool isBoardInitialized = false;
    private bool isTimeUp = false;
    private Queue<int> scoreValues = new Queue<int>();
    private int displayedScore;

    /// <summary>
    /// Initialize GuiManager.
    /// </summary>
    private void Start()
    {
        guiManager = gameObject.AddComponent<GuiManager>();
        guiManager.Init(isMoveLimitSet, isTimeLimitSet);
        guiManager.SetTextValue("ScoreText", "0");

        if (isMoveLimitSet)
        {
            guiManager.SetTextValue("MovesText", remainingMoves.ToString());
        }

        if (isTimeLimitSet)
        {
            guiManager.SetTextValue("TimeText", TimeToReadableFormat(remainingTime));
            StartCoroutine(CheckTimeLimit());
        }

        StartCoroutine(CountUpToScore());
    }

    /// <summary>
    /// Handle time limit and escape key event.
    /// </summary>
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("LevelSelect");
        }
    }

    private IEnumerator CheckTimeLimit()
    {
        while (isTimeLimitSet)
        {
            yield return new WaitForSeconds(1f);
            remainingTime--;

            if (remainingTime <= 0f)
            {
                isTimeUp = true;
                guiManager.SetTextValue("TimeText", TimeToReadableFormat(0f));
                isTimeLimitSet = false;

                new GameOver()
                {
                    hasWon = false,
                    heading = "You Lose",
                    description = "You have run out of time!"
                }.Fire();
            }
            else
            {
                guiManager.SetTextValue("TimeText", TimeToReadableFormat(remainingTime));
            }
        }
    }

    /// <summary>
    /// Event handler for home button. Return to level selection screen.
    /// </summary>
    public void OnHomeButtonClick()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("LevelSelect");
    }

    /// <summary>
    /// Reset score field and GUI text.
    /// </summary>
    public void ResetScore()
    {
        scoreValues.Clear();
        score = 0;
        guiManager.SetTextValue("ScoreText", score.ToString());
    }

    /// <summary>
    /// Add event handlers for messages.
    /// </summary>
    private void OnEnable()
    {
        AddScore.AddListener(OnAddScore);
        GameOver.AddListener(OnGameOver);
        BoardInitialized.AddListener(OnBoardInitialized);
        ValidMove.AddListener(OnValidMove);
    }

    private void OnGameOver(GameOver eventData)
    {
        isTimeLimitSet = false;
    }

    private void OnAddScore(AddScore eventData)
    {
        if (isBoardInitialized && !isTimeUp)
        {
            score += eventData.value;
            scoreValues.Enqueue(score);
            CustomLogger.Log(string.Format("New score value: {0}", score), LogPriority.Info);
        }
    }

    /// <summary>
    /// Remove event handlers for messages.
    /// </summary>
    private void OnDisable()
    {
        AddScore.RemoveListener(OnAddScore);
        GameOver.RemoveListener(OnGameOver);
        BoardInitialized.RemoveListener(OnBoardInitialized);
        ValidMove.RemoveListener(OnValidMove);
    }

    private IEnumerator CountUpToScore()
    {
        int nextValue = 0;

        while (!isTimeUp)
        {
            if (scoreValues.Count > 0)
            {
                nextValue = scoreValues.Dequeue();

                if (scoreValues.Count == 0)
                {
                    guiManager.AnimateScore();
                }

                while (displayedScore < nextValue)
                {
                    displayedScore += 10;
                    guiManager.SetTextValue("ScoreText", displayedScore.ToString());
                    yield return null;
                }

                displayedScore = nextValue;
                guiManager.SetTextValue("ScoreText", displayedScore.ToString());

                if (isScoreLimitSet && score >= scoreLimit)
                {
                    new GameOver()
                    {
                        hasWon = true,
                        heading = "You Win",
                        description = "You have reached the target score!"
                    }.Fire();

                    isTimeLimitSet = false;
                    isMoveLimitSet = false;
                }
            }

            yield return null;
        }
    }

    /// <summary>
    /// Event handler for board initialized message. Set flag to true.
    /// </summary>
    private void OnBoardInitialized(BoardInitialized eventData)
    {
        isBoardInitialized = true;
    }

    /// <summary>
    /// Valid move event handler. Update GUI and check if the move limit has been reached.
    /// </summary>
    private void OnValidMove(ValidMove eventData)
    {
        if (isMoveLimitSet)
        {
            remainingMoves--;
            guiManager.SetTextValue("MovesText", remainingMoves.ToString());

            if (remainingMoves == 0)
            {
                isTimeLimitSet = false;

                new GameOver()
                {
                    hasWon = false,
                    heading = "You Lose",
                    description = "You have no moves left!"
                }.Fire();
            }
        }
    }

    /// <summary>
    /// Helper method to convert time float value to a more readable format.
    /// </summary>
    /// <param name="time">Time in seconds</param>
    /// <returns>A formatted time string (mm:ss)</returns>
    private string TimeToReadableFormat(float time)
    {
        int minutes = Mathf.FloorToInt(time / 60f);
        int seconds = Mathf.FloorToInt(time - minutes * 60);
        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
