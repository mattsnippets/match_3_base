﻿using Mattsnippets.EventSystem;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// GUI manager class.
/// </summary>
public class GuiManager : MonoBehaviour
{
    [SerializeField]
    private GameObject gui;

    private CanvasGroup gameOverGroup;
    private Dictionary<string, Text> guiTexts = new Dictionary<string, Text>();

    /// <summary>
    /// Find all text elements and create a dictionary indexed with their names. Disable/enable move
    /// and time counter as needed.
    /// </summary>
    /// <param name="isMoveLimitSet">Input from GameManager indicating if move limit is set</param>
    /// <param name="isTimeLimitSet">Input from GameManager indicating if time limit is set</param>
    public void Init(bool isMoveLimitSet, bool isTimeLimitSet)
    {
        gui = GameObject.Find("Gui");
        List<Text> foundTexts = new List<Text>();
        GameObject.Find("Gui").GetComponentsInChildren(foundTexts);

        foreach (Text text in foundTexts)
        {
            guiTexts.Add(text.name, text);
        }

        if (!isMoveLimitSet)
        {
            DisableTextElement("Move");
        }

        if (!isTimeLimitSet)
        {
            DisableTextElement("Time");
        }

        gameOverGroup = gui.GetComponentInChildren<CanvasGroup>();
    }

    /// <summary>
    /// Find a text element by name and disable it.
    /// </summary>
    /// <param name="name">Name of text element</param>
    private void DisableTextElement(string name)
    {
        foreach (KeyValuePair<string, Text> textElement in guiTexts.Where(t => t.Key.Contains(name)))
        {
            textElement.Value.transform.parent.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Find a text element by name and set its value.
    /// </summary>
    /// <param name="name">Name of text element</param>
    /// <param name="value">Value of text element</param>
    public void SetTextValue(string name, string value)
    {
        guiTexts[name].text = value;
    }

    public void AnimateScore()
    {
        guiTexts["ScoreText"].GetComponent<Animator>().SetTrigger("Throb");
    }

    /// <summary>
    /// Add event handlers for game over events.
    /// </summary>
    private void OnEnable()
    {
        GameOver.AddListener(OnGameOver);
    }

    /// <summary>
    /// Remove event handlers for game over events.
    /// </summary>
    private void OnDisable()
    {
        GameOver.RemoveListener(OnGameOver);
    }

    private void OnGameOver(GameOver eventData)
    {
        SetTextValue("BigGameOverText", eventData.heading);
        SetTextValue("SmallGameOverText", eventData.description);
        gameOverGroup.alpha = 1f;
        gameOverGroup.interactable = true;
    }
}
