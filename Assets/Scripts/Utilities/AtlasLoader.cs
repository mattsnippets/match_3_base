﻿using UnityEngine;
using UnityEngine.U2D;

public class AtlasLoader : MonoBehaviour
{
    [SerializeField]
    private SpriteAtlas piecesHd;
    [SerializeField]
    private SpriteAtlas piecesSd;
    [SerializeField]
    private SpriteAtlas miscHd;
    [SerializeField]
    private SpriteAtlas miscSd;

    private float nativeWidth;

    private void Awake()
    {
        nativeWidth = Screen.width;
    }

    private void OnEnable()
    {
        SpriteAtlasManager.atlasRequested += RequestAtlas;
    }

    private void OnDisable()
    {
        SpriteAtlasManager.atlasRequested -= RequestAtlas;
    }

    private void RequestAtlas(string tag, System.Action<SpriteAtlas> callback)
    {
        switch (tag)
        {
            case "Pieces":
                callback(nativeWidth >= 1080f ? piecesHd : piecesSd);
                break;
            case "Misc":
                callback(nativeWidth >= 1080f ? miscHd : miscSd);
                break;
            default:
                CustomLogger.Log(string.Format("Unknown sprite atlas requested: {0}", tag), LogPriority.Warning);
                break;
        }

    }
}