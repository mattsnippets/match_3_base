﻿using UnityEngine;

public class ResolutionSetter : MonoBehaviour
{
    private void Start()
    {
#if UNITY_ANDROID
        Screen.SetResolution(Screen.width / 2, Screen.height / 2, true);
        Application.targetFrameRate = 30;
#endif
        Application.targetFrameRate = 60;
        //Screen.SetResolution(360, 640, true);
    }
}
