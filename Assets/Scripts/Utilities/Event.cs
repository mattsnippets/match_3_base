﻿namespace Mattsnippets.EventSystem
{
    public abstract class Event<T> where T : Event<T>
    {
        public string Description { get; set; }

        public delegate void EventListener(T eventData);
        private static event EventListener listeners;

        public static void AddListener(EventListener listener)
        {
            listeners += listener;
        }

        public static void RemoveListener(EventListener listener)
        {
            listeners -= listener;
        }

        public void Fire()
        {
            CustomLogger.Log( $"Event fired: {GetType().Name}, {Description}", LogPriority.Info);
            listeners?.Invoke(this as T);
        }
    }
}