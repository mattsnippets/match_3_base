﻿using UnityEngine;

namespace Mattsnippets.EventSystem
{
    public class HeroAttachStateChanged : Event<HeroAttachStateChanged>
    {
        public bool IsAttached;
        public Transform currentHero;
    }

    public class AddScore : Event<AddScore>
    {
        public int value;
    }

    public class GameOver : Event<GameOver>
    {
        public bool hasWon;
        public string heading;
        public string description;
    }

    public class BoardInitialized : Event<BoardInitialized> { }

    public class ValidMove : Event<ValidMove> { }

    public class MoveHintStateChanged : Event<MoveHintStateChanged>
    {
        public bool isActive;
        public CoordinatePair hintCoords;
    }

    public class MusicEnabled : Event<MusicEnabled>
    {
        public bool isEnabled;
    }
}
