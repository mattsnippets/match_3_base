﻿using System;
using UnityEngine;

[Flags]
public enum LogPriority
{
    Info = 1,
    Warning = 2,
    Error = 4
}

/// <summary>
/// Custom logger class automatically disabled when not in debug mode.
/// </summary>
public class CustomLogger : MonoBehaviour
{
    [SerializeField]
    private bool isEnabled;

    public static LogPriority PriorityFilter;
    public LogPriority PriorityFilterFromEditor;
    private static bool logEnabled;
    private static int enableButtonClicks = 0;
    private static bool loggerCreated;

    /// <summary>
    /// Logging method which wraps Debug.Log functionality of Unity. Only logs selected priorities
    /// and shows stack trace information as well.
    /// </summary>
    /// <param name="message">Log message</param>
    /// <param name="priority">Priority of log</param>    
    public static void Log(string message, LogPriority priority)
    {
        if (logEnabled)
        {
            if (!loggerCreated)
            {
                Debug.LogWarning("Attempted logging with no logger object created. Attach CustomLogger script to GameObject.");
            }

            if (((byte)priority & (byte)PriorityFilter) != 0)
            {
                string stackTrace = StackTraceUtility.ExtractStackTrace()
                    .Split('\n')[1];

                switch (priority)
                {
                    case LogPriority.Info:
                        Debug.Log(message + Environment.NewLine + stackTrace);
                        break;
                    case LogPriority.Warning:
                        Debug.LogWarning(message + Environment.NewLine + stackTrace);
                        break;
                    case LogPriority.Error:
                        Debug.LogError(message + Environment.NewLine + stackTrace);
                        break;
                }
            }
        }
    }

    public void EnableDebug()
    {
        //enableButtonClicks++;

        //if (enableButtonClicks == 8)
        //{
        //    logEnabled = true;
        //    Debug.Log("Logging enabled from menu.");
        //    Messenger.Broadcast(GameEvent.DebugLogEnabled);
        //}
    }

    /// <summary>
    /// Assign filter setting from custom editor the static priority filter value.
    /// </summary>    
    private void Awake()
    {
#if DEBUG
        logEnabled = true;
#endif

        logEnabled = isEnabled;
        Debug.unityLogger.logEnabled = logEnabled;
        PriorityFilter = PriorityFilterFromEditor;
        DontDestroyOnLoad(gameObject);

        if(!logEnabled)
        {
            Debug.Log("Logging is disabled.");
        }

        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }

        loggerCreated = true;
    }
}