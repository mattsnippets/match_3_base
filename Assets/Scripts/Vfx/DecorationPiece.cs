﻿using System.Collections;
using UnityEngine;

public class DecorationPiece : MonoBehaviour
{
    [SerializeField]
    private Sprite[] sprites;
    [SerializeField]
    private float lifeSpan = 10f;

    private SpriteRenderer spriteRenderer;
    private Rigidbody2D rigidbody2d;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        spriteRenderer.sprite = sprites[Random.Range(0, sprites.Length)];
        rigidbody2d.velocity = new Vector2(0f, -3f);
        rigidbody2d.AddTorque(Random.Range(-40f, 40f));
        StartCoroutine(Disable());
    }

    private IEnumerator Disable()
    {
        yield return new WaitForSeconds(lifeSpan);
        gameObject.SetActive(false);
    }
}
