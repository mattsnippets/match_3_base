﻿using System.Collections;
using UnityEngine;

public class DecorationPieceSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject decorationPiecePrefab;
    [SerializeField]
    private Camera camera;

    private float halfScreenWidth;

    private void Start()
    {
        halfScreenWidth = camera.pixelWidth / 3f; 
        StartCoroutine(SpawnPiece());
    }

    private IEnumerator SpawnPiece()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.3f);
            GameObject decorationPieceGo = ObjectPoolManager.Current.GetPooledObject(decorationPiecePrefab);
            decorationPieceGo.transform.parent = transform;
            decorationPieceGo.transform.localPosition = new Vector3(Random.Range(-halfScreenWidth, halfScreenWidth), 0f, -3f);
            decorationPieceGo.SetActive(true);
        }
    }
}
