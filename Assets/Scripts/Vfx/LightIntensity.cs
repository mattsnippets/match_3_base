﻿using System;
using System.Collections;
using UnityEngine;

public class LightIntensity : MonoBehaviour
{
    [SerializeField]
    private AnimationCurve intensityCurve;
    [SerializeField]
    private float minIntensity;
    [SerializeField]
    private float maxIntensity;
    [SerializeField]
    private float instensityCycleLength;

    private Light light;

    private void Awake()
    {
        light = GetComponent<Light>();

        if (Convert.ToBoolean(PlayerPrefs.GetInt("FlickerLightsEnabled", 1)))
        {
            StartCoroutine(ChangeIntensity());
        }
    }

    private IEnumerator ChangeIntensity()
    {
        float elapsedTime = 0f;

        while (true)
        {
            float intensity = Mathf.Lerp(minIntensity, maxIntensity, intensityCurve
                .Evaluate(elapsedTime / instensityCycleLength));
            light.intensity = intensity;
            elapsedTime += Time.deltaTime;
            elapsedTime %= instensityCycleLength;
            yield return null;
        }
    }
}
