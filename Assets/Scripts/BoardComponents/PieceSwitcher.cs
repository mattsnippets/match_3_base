﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Class responsible for piece switching logic.
/// </summary>
public class PieceSwitcher : MonoBehaviour
{
    [SerializeField]
    private float switchSpeed;

    private IEnumerable<Piece> selectedMatches;
    private IEnumerable<Piece> targetMatches;
    private IEnumerable<Piece> colorBombMatches;
    private Action<IEnumerable<Piece>, BombData, BombData> switchFinished;
    private BombData selectedTileBomb;
    private BombData targetTileBomb;
    private MatchFinder matchFinder;
    private PieceManager pieceManager;
    private Piece selectedPiece;
    private Piece targetPiece;
    private Tile selectedTile;
    private Tile targetTile;
    private bool isValidSwitch;

    /// <summary>
    /// Initialize PieceSwitcher with a callback for returning values, a MatchFinder and a PieceManager
    /// reference.
    /// </summary>
    /// <param name="switchFinished">Callback to invoke after switching has finished</param>
    /// <param name="matchFinder">MatchFinder reference</param>
    /// <param name="pieceManager">PieceManager reference</param>
    public void Init(Action<IEnumerable<Piece>, BombData, BombData> switchFinished,
        MatchFinder matchFinder, PieceManager pieceManager)
    {
        this.switchFinished = switchFinished;
        this.matchFinder = matchFinder;
        this.pieceManager = pieceManager;
    }

    /// <summary>
    /// Try a piece switch by invoking MakeSwitchCoroutine(), check for matches, if there aren't any,
    /// set invalid switch flag and also call RevertSwitchCoroutine(). Finish with
    /// ReturnSwitchResultsCoroutine().
    /// </summary>
    /// <param name="selectedPiece">The piece selected by clicking</param>
    /// <param name="targetPiece">The piece which the piece has been dragged towards</param>
    /// <param name="selectedTile">The tile where the player clicked first</param>
    /// <param name="targetTile">The tile which the piece has been dragged towards</param>
    /// <returns>Coroutine IEnumerator</returns>
    public IEnumerator SwitchPieces(Piece selectedPiece, Piece targetPiece, Tile selectedTile,
        Tile targetTile)
    {
        isValidSwitch = true;
        this.selectedPiece = selectedPiece;
        this.targetPiece = targetPiece;
        this.selectedTile = selectedTile;
        this.targetTile = targetTile;

        CustomLogger.Log(string.Format("Making switch: selected piece: {0} {1} target piece: {2} {3}",
            selectedPiece.X, selectedPiece.Y, targetPiece.X, targetPiece.Y), LogPriority.Info);

        yield return StartCoroutine(MakeSwitchCoroutine());

        if (!selectedMatches.Any() && !targetMatches.Any() && !colorBombMatches.Any())
        {
            isValidSwitch = false;
            yield return StartCoroutine(RevertSwitchCoroutine());
        }

        yield return StartCoroutine(ReturnSwitchResultsCoroutine());
    }

    /// <summary>
    /// Switch the two pieces then find all matches at both locations, also check for color bomb matches.
    /// </summary>
    /// <returns>Coroutine IEnumerator</returns>
    private IEnumerator MakeSwitchCoroutine()
    {
        selectedPiece.Move(targetTile.X, targetTile.Y, switchSpeed);
        targetPiece.Move(selectedTile.X, selectedTile.Y, switchSpeed);

        yield return new WaitForSeconds(switchSpeed + 0.2f);

        selectedMatches = matchFinder.FindMatchesAt(selectedTile.X, selectedTile.Y,
            pieceManager.PieceArrayWrapper);
        targetMatches = matchFinder.FindMatchesAt(targetTile.X, targetTile.Y,
            pieceManager.PieceArrayWrapper);
        colorBombMatches = pieceManager.GetColorBombMatches(selectedPiece, targetPiece);
    }

    /// <summary>
    /// Revert an invalid switch, move back the pieces to their original place.
    /// </summary>
    /// <returns>Coroutine IEnumerator</returns>
    private IEnumerator RevertSwitchCoroutine()
    {
        selectedPiece.Move(selectedTile.X, selectedTile.Y, switchSpeed);
        targetPiece.Move(targetTile.X, targetTile.Y, switchSpeed);

        yield return new WaitForSeconds(switchSpeed);
    }

    /// <summary>
    /// If the switch is valid create bombdata for the pieces and union all the matches as a single
    /// returned collection. Call switchFinished with these arguments. If switch is invalid, all
    /// arguments are null.
    /// </summary>
    /// <returns>Coroutine IEnumerator</returns>
    private IEnumerator ReturnSwitchResultsCoroutine()
    {
        yield return new WaitForSeconds(switchSpeed);

        if (isValidSwitch)
        {
            Vector2 switchDirection = new Vector2(targetTile.X - selectedTile.X, targetTile.Y -
                selectedTile.Y);

            selectedTileBomb = pieceManager.CreateBombData(selectedTile.X, selectedTile.Y,
                switchDirection, selectedMatches);
            targetTileBomb = pieceManager.CreateBombData(targetTile.X, targetTile.Y, switchDirection,
                targetMatches);

            switchFinished(selectedMatches.Union(targetMatches).Union(colorBombMatches),
                selectedTileBomb, targetTileBomb);
        }
        else
        {
            switchFinished(null, null, null);
        }
    }
}
