﻿using UnityEngine;

[System.Serializable]
public struct PresetBoardElement
{
    public int x;
    public int y;
    public GameObject elementPrefab;
}

public class ArrayWrapper<T>
{
    private T[,] data;

    public ArrayWrapper(T[,] data)
    {
        this.data = data;
    }

    public ArrayWrapper(ArrayWrapper<T> other)
    {
        this.data = other.data.Clone() as T[,];
    }

    public T this[int i, int j]
    {
        get
        {
            return data[i, j];
        }
    }

    public void SetElement(int i, int j, T value)
    {
        data[i, j] = value;
    }

    public int Count
    {
        get
        {
            return data.Length;
        }
    }

    public int GetLength(int dimension)
    {
        return data.GetLength(dimension);
    }
}

public struct CoordinatePair
{
    public CoordinatePair(int coord1X, int coord1Y, int coord2X, int coord2Y)
    {
        coord1 = new Vector2(coord1X, coord1Y);
        coord2 = new Vector2(coord2X, coord2Y);
    }

    public Vector2 coord1;
    public Vector2 coord2;
}