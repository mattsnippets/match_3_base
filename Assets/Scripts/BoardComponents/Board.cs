﻿using Mattsnippets.EventSystem;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

/// <summary>
/// Main component of game logic containing smaller components for gameplay management.
/// </summary>
public class Board : MonoBehaviour
{
    [SerializeField]
    private float playAreaBorderSize;
    [SerializeField]
    private CameraManager cameraManager;
    [SerializeField]
    private bool isAutoTestEnabled;

    [SerializeField]
    private GameObject basicTileLightPrefab;
    [SerializeField]
    private GameObject basicTileDarkPrefab;
    [SerializeField]
    private GameObject obstacleTilePrefab;
    [SerializeField]
    private PresetBoardElement[] presetTiles;
    [SerializeField]
    private PresetBoardElement[] presetPieces;
    [SerializeField]
    private float hintMarkerTimeout = 15f;
    [SerializeField]
    private GameObject verticalTorchPrefab;
    [SerializeField]
    private GameObject horizontalTorchPrefab;
    [SerializeField]
    private GameObject obeliskPrefab;

    private static int width = 8;
    private static int height = 8;
    private int basicPieceTypeCount = 6;

    private Tile[,] tiles;
    private Tile selectedTile;
    private Tile targetTile;

    private MatchFinder matchFinder;
    private InputHandler inputHandler;
    private ColumnManager columnManager;
    private PieceManager pieceManager;
    private AutoTester autoTester;
    private PieceSwitcher pieceSwitcher;
    private MoveFinder moveFinder;

    private bool isInputEnabled = true;
    private int scoreMultiplier;
    private bool isClearRefillInProgress;
    private bool isPieceRefillEnabled = true;
    private bool playerMoved;

    private CoordinatePair moveHint;
    private BombData selectedTileBomb;
    private BombData targetTileBomb;

    public AutoTester AutoTester
    {
        get
        {
            return autoTester;
        }
    }

    public static bool IsWithinBoard(int x, int y)
    {
        return (x >= 0 && x < width && y >= 0 && y < height);
    }

    private void Start()
    {
        tiles = new Tile[width, height];

        InitializeComponents();
        InitializeTiles();
        FillAtStart();

        if (Screen.height > Screen.width)
        {
            CreateVerticalTorch(new Vector2(0f, -3f), true);
            CreateVerticalTorch(new Vector2(7f, -3f), false);
            CreateVerticalTorch(new Vector2(0f, 8f), true);
            CreateVerticalTorch(new Vector2(7f, 8f), false);
        }
        else
        {
            CreateHorizontalTorch(new Vector2(-2f, 0f));
            CreateHorizontalTorch(new Vector2(9f, 0f));
            CreateHorizontalTorch(new Vector2(-2f, 6f));
            CreateHorizontalTorch(new Vector2(9f, 6f));
            Instantiate(obeliskPrefab, new Vector3(-2f, -3f, -2.64f), Quaternion.identity, transform);
            Instantiate(obeliskPrefab, new Vector3(9f, -3f, -2.64f), Quaternion.identity, transform);
        }

        new BoardInitialized().Fire();

        InitAutoTester();
        moveHint = moveFinder.FindMove();

        GetComponent<ParticleManager>().IsEnabled = true;
        StartCoroutine(CheckIfMoveMade());
    }

    /// <summary>
    /// Initialize PieceManager, CameraManager, MatchFinder, InputHandler and ColumnManager.
    /// </summary>
    private void InitializeComponents()
    {
        pieceManager = GetComponent<PieceManager>();
        pieceManager.Init(width, height);

        cameraManager.AdjustCamera(width, height + 1, playAreaBorderSize);

        matchFinder = new MatchFinder(width, height);

        inputHandler = new InputHandler(selectedTile, targetTile,
            (selectedTile, targetTile) =>
            {
                TrySwitch(selectedTile, targetTile);
            }
        );

        columnManager = new ColumnManager(width, height, pieceManager.PieceArrayWrapper,
            new ArrayWrapper<Tile>(tiles));

        pieceSwitcher = GetComponent<PieceSwitcher>();
        pieceSwitcher.Init(TrySwitchFinished, matchFinder, pieceManager);

        moveFinder = new MoveFinder(width, height, pieceManager, matchFinder);
    }

    private void CreateVerticalTorch(Vector2 pos, bool isLeft)
    {
        GameObject torchGo = Instantiate(verticalTorchPrefab, new Vector3(pos.x, pos.y, -4f), Quaternion.identity, transform);

        if (!isLeft)
        {
            torchGo.transform.Rotate(new Vector3(11f, 180f, 0f));
        }
        else
        {
            torchGo.transform.Rotate(new Vector3(-11f, 0f, 0f));
        }
    }

    private void CreateHorizontalTorch(Vector2 pos)
    {
        Instantiate(horizontalTorchPrefab, new Vector3(pos.x, pos.y, -4f), Quaternion.Euler(-11f, 0f, 0f), transform);
    }

    /// <summary>
    /// Add event listeners on enable.
    /// </summary>
    private void OnEnable()
    {
        GameOver.AddListener(OnGameOver);
    }

    /// <summary>
    /// Remove event listeners on disable.
    /// </summary>
    private void OnDisable()
    {
        GameOver.RemoveListener(OnGameOver);
    }

    /// <summary>
    /// Instantiate an AutoTester. If auto test is enabled, start its automatic play coroutine.
    /// </summary>
    private void InitAutoTester()
    {
        autoTester = new AutoTester(inputHandler, pieceManager, tiles, width, height,
                () =>
                {
                    return isClearRefillInProgress;
                },

                (bool isPieceRefillEnabled) =>
                {
                    this.isPieceRefillEnabled = isPieceRefillEnabled;
                }, moveFinder);

        if (isAutoTestEnabled)
        {
            StartCoroutine(autoTester.TestMoveCoroutine());
        }
    }

    /// <summary>
    /// Load preset tiles first, then create remaining tiles with a checker pattern.
    /// </summary>
    private void InitializeTiles()
    {
        foreach (PresetBoardElement tile in presetTiles)
        {
            CreateTile(tile.x, tile.y, tile.elementPrefab);
        }

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (tiles[i, j] == null)
                {
                    if ((i + j) % 2 == 0)
                    {
                        CreateTile(i, j, basicTileLightPrefab);
                    }
                    else
                    {
                        CreateTile(i, j, basicTileDarkPrefab);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Create tile at given position, store its script component in the tiles array, set the board
    /// as its parent, call Init() on tile.
    /// </summary>
    /// <param name="x">X coordinate</param>
    /// <param name="y">Y coordinate</param>
    /// <param name="tilePrefab">The tile prefab to instantiate</param>
    private void CreateTile(int x, int y, GameObject tilePrefab)
    {
        GameObject tile = Instantiate(tilePrefab, new Vector3(x, y, tilePrefab.GetComponent<Tile>()
            .Type == TileType.Obstacle ? -2.3f : 0f), Quaternion.identity);
        tile.name = x + "," + y;
        tiles[x, y] = tile.GetComponent<Tile>();
        tile.transform.parent = transform;
        tiles[x, y].Init(x, y, inputHandler);
    }

    /// <summary>
    /// Fill the board first with the preset pieces then fill the rest with random pieces (except
    /// at obstacle tiles) such that they don't create a match.
    /// </summary>
    private void FillAtStart()
    {
        foreach (PresetBoardElement piece in presetPieces)
        {
            pieceManager.CreatePiece((int)piece.elementPrefab.GetComponent<Piece>().Type, piece.x, piece.y);
        }

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (pieceManager.PieceArrayWrapper[i, j] == null && tiles[i, j].Type != TileType.Obstacle)
                {
                    if (i == 0 && j == 0)
                    {
                        pieceManager.CreatePiece(Random.Range(0, basicPieceTypeCount), i, j);
                    }
                    else
                    {
                        pieceManager.CreatePiece(Random.Range(0, basicPieceTypeCount), i, j);

                        while (matchFinder.FindMatchesAt(i, j, pieceManager.PieceArrayWrapper).Any())
                        {
                            pieceManager.RemovePiece(i, j);
                            pieceManager.CreatePiece(Random.Range(0, basicPieceTypeCount), i, j);
                        }
                    }
                }
            }
        }

        Assert.IsFalse(matchFinder.FindAllMatches(pieceManager.PieceArrayWrapper).Any(),
            "There are matches in the start board!");
    }

    /// <summary>
    /// Fill empty holes in board with pieces instantiated outside the play area so that they can
    /// fall into place.
    /// </summary>
    private void FillEmptyPieces()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (pieceManager.PieceArrayWrapper[i, j] == null && tiles[i, j].Type != TileType.Obstacle)
                {
                    pieceManager.CreatePiece(Random.Range(0, basicPieceTypeCount), i, j, 5,
                      (j + 1) * 0.05f);
                }
            }
        }
    }

    /// <summary>
    /// Attempt to switch two gamepieces. If they are not null, input is disabled and PieceSwitcher's
    /// SwitchPieces() method is called.
    /// </summary>
    /// <param name="selectedTile">The tile where the player clicked first</param>
    /// <param name="targetTile">The tile which the piece has been dragged towards</param>
    private void TrySwitch(Tile selectedTile, Tile targetTile)
    {
        if (isInputEnabled)
        {
            Piece selectedPiece = pieceManager.PieceArrayWrapper[selectedTile.X, selectedTile.Y];
            Piece targetPiece = pieceManager.PieceArrayWrapper[targetTile.X, targetTile.Y];

            if (selectedPiece != null && targetPiece != null)
            {
                isInputEnabled = false;
                isClearRefillInProgress = true;
                playerMoved = true;
                ResetHintMarkers();

                StartCoroutine(pieceSwitcher.SwitchPieces(selectedPiece, targetPiece, selectedTile,
                    targetTile));
            }
        }
    }

    /// <summary>
    /// Method used as a callback by PieceSwitcher. If there are matches bombs are set and 
    /// ClearRefillCoroutine() is invoked. If there are no matches, input is enabled again and 
    /// isClearRefillInProgress is set to false.
    /// </summary>
    /// <param name="matches">A collection of matching Pieces</param>
    /// <param name="selectedTileBomb">BombData for the selected tile</param>
    /// <param name="targetTileBomb">BombData for the target tile</param>
    private void TrySwitchFinished(IEnumerable<Piece> matches, BombData selectedTileBomb,
        BombData targetTileBomb)
    {
        if (matches != null)
        {
            this.selectedTileBomb = selectedTileBomb;
            this.targetTileBomb = targetTileBomb;
            StartCoroutine(ClearRefillCoroutine(matches));
        }
        else
        {
            isInputEnabled = true;
            isClearRefillInProgress = false;
        }
    }

    /// <summary>
    /// Reset hint marker timers and deactivate markers when the player has made a move.
    /// </summary>
    private void ResetHintMarkers()
    {
        new MoveHintStateChanged()
        {
            isActive = false
        }.Fire();
    }

    /// <summary>
    /// Invoke ClearCoroutine() first with the input Pieces then with any remaining matches on the 
    /// board. After every clear phase invoke FillEmptyPieces(). Broadcast a valid move message, 
    /// check if there are any possible moves then enable input and set isClearRefillInProgress to
    /// false.
    /// </summary>
    /// <param name="pieces">Collection of pieces to be removed first</param>
    /// <returns>Coroutine IEnumerator</returns>
    private IEnumerator ClearRefillCoroutine(IEnumerable<Piece> pieces)
    {
        IEnumerable<Piece> matches = pieces;
        scoreMultiplier = 0;
        float refillWaitTime = ((height + 1) * 0.05f) + 0.05f;

        do
        {
            scoreMultiplier++;

            yield return StartCoroutine(ClearCoroutine(matches));

            if (isPieceRefillEnabled)
            {
                FillEmptyPieces();
            }

            matches = matchFinder.FindAllMatches(pieceManager.PieceArrayWrapper);

            yield return new WaitForSeconds(refillWaitTime);

        }
        while (matches.Any());

        new ValidMove().Fire();

        moveHint = moveFinder.FindMove();

        isInputEnabled = true;
        isClearRefillInProgress = false;
    }

    /// <summary>
    /// Main clearing logic called recursively. Invoke ClearPieces(), create bombs when needed, wait 
    /// for pieces to collapse. If new matches have been formed during the first clear iteration, 
    /// call ClearCoroutine() recursively again.
    /// </summary>
    /// <param name="pieces">Collection of pieces to be removed first</param>
    /// <returns>Coroutine IEnumerator</returns>
    private IEnumerator ClearCoroutine(IEnumerable<Piece> pieces)
    {
        HashSet<Piece> matches;
        bool isDone = false;

        while (!isDone)
        {
            pieces = ClearPieces(pieces);

            yield return new WaitForSeconds(0.1f);

            CreateBomb(ref selectedTileBomb);
            CreateBomb(ref targetTileBomb);

            matches = new HashSet<Piece>();

            yield return StartCoroutine(CollapsePiecesCoroutine(pieces, matches));

            if (!matches.Any())
            {
                isDone = true;
                break;
            }
            else
            {
                scoreMultiplier++;
                yield return StartCoroutine(ClearCoroutine(matches));
            }
        }

        yield return null;
    }

    /// <summary>
    /// Clear pieces from the input unioned with pieces destroyed by bombs. Explosions are calculated
    /// via calling ExplodeBombs(), pieces are removed by PieceManager.
    /// </summary>
    /// <param name="pieces">Initial list of pieces to be removed</param>
    /// <returns>Coroutine IEnumerator</returns>
    private IEnumerable<Piece> ClearPieces(IEnumerable<Piece> pieces)
    {
        HashSet<Piece> destroyedPieces = new HashSet<Piece>();
        ExplodeBombs(pieces, destroyedPieces);
        pieces = pieces.Union(destroyedPieces);
        pieceManager.RemovePiece(pieces, scoreMultiplier);
        return pieces;
    }

    /// <summary>
    /// If the input BombData is not null, create a piece with the desired type, position and attached
    /// bomb object then set the input BombData to null.
    /// </summary>
    /// <param name="bombData">A BombData class containing all the information for the created 
    /// bomb</param>
    private void CreateBomb(ref BombData bombData)
    {
        if (bombData != null)
        {
            pieceManager.CreatePiece((int)bombData.createdPieceType, bombData.x, bombData.y);
            pieceManager.AttachBomb(bombData.x, bombData.y, bombData.bombType);
            bombData = null;
        }
    }

    /// <summary>
    /// Call ColumnManager's SlideColumns() to move pieces into their places, wait until all pieces
    /// finish their traversal. Check for new matches in the changed piece setup and add it to the 
    /// matches set.
    /// </summary>
    /// <param name="pieces">Collection of pieces to collapse</param>
    /// <param name="matches">A HashSet serving as a return value from the coroutine. Contains
    ///  new matches formed during the slide phase</param>
    /// <returns>Coroutine IEnumerator</returns>
    private IEnumerator CollapsePiecesCoroutine(IEnumerable<Piece> pieces, HashSet<Piece> matches)
    {
        IEnumerable<Piece> movedPieces = new List<Piece>();
        movedPieces = columnManager.SlideColumns(pieces);

        while (!AreCollapsed(pieces))
        {
            yield return null;
        }

        yield return new WaitForSeconds(0.1f);

        foreach (Piece piece in movedPieces)
        {
            matches.UnionWith(matchFinder.FindMatchesAt(piece.X, piece.Y,
                pieceManager.PieceArrayWrapper));
        }
    }

    /// <summary>
    /// Check if all the pieces in the input collection reached their destination y coordinate.
    /// </summary>
    /// <param name="pieces">A collection of pieces</param>
    /// <returns>True if all the pieces are at their destination y coordinate</returns>
    private bool AreCollapsed(IEnumerable<Piece> pieces)
    {
        foreach (Piece piece in pieces)
        {
            if (piece != null)
            {
                if (piece.transform.position.y - (float)piece.Y > 0.001f)
                {
                    return false;
                }
            }
        }

        return true;
    }

    /// <summary>
    /// Gather all pieces effected by bomb explosions. Find all pieces with bombs in the input, "explode"
    /// the bombs by calling GetExplodedPieces() then continue the chain reaction by calling ExplodeBombs()
    /// repeatedly until all the effected pieces are in the allPieces set.
    /// </summary>
    /// <param name="pieces">Initial collection of pieces that may contain bombs</param>
    /// <param name="allPieces">A HashSet containing all pieces involved in the explosions</param>
    private void ExplodeBombs(IEnumerable<Piece> pieces, HashSet<Piece> allPieces)
    {
        HashSet<Piece> explodedPieces = new HashSet<Piece>();

        IEnumerable<Piece> piecesWithBomb = pieces.Where(piece => piece != null &&
        piece.GetComponentInChildren<Bomb>() != null);

        foreach (Piece piece in piecesWithBomb)
        {
            explodedPieces.UnionWith(GetExplodedPieces(piece));
        }

        IEnumerable<Piece> effectedPieces = explodedPieces.Except(piecesWithBomb);

        if (explodedPieces.Except(allPieces).Any())
        {
            allPieces.UnionWith(explodedPieces);
            ExplodeBombs(effectedPieces, allPieces);
        }
    }

    /// <summary>
    /// Return a collection of pieces based on the input Piece's bomb type and location. The collection
    /// is acquired via calling the corresponding method in PieceManager.
    /// </summary>
    /// <param name="bombPiece">The piece with the exploded bomb</param>
    /// <returns>A collection of pieces destroyed by the explosion</returns>
    private IEnumerable<Piece> GetExplodedPieces(Piece bombPiece)
    {
        IEnumerable<Piece> explodedPieces;

        switch (bombPiece.GetComponentInChildren<Bomb>().BombType)
        {
            case BombType.Block:
                explodedPieces = pieceManager.GetPieceBlock(bombPiece.X, bombPiece.Y, 1);
                break;
            case BombType.Row:
                explodedPieces = pieceManager.GetPieceRow(bombPiece.Y);
                break;
            case BombType.Column:
                explodedPieces = pieceManager.GetPieceColumn(bombPiece.X);
                break;
            case BombType.Color:
                // Color bomb logic is handled at piece switch, return empty list
                explodedPieces = new List<Piece>();
                break;
            default:
                CustomLogger.Log("Unknown bomb type!", LogPriority.Error);
                explodedPieces = null;
                break;
        }

        return explodedPieces;
    }

    private void OnGameOver(GameOver eventData)
    {
        if (eventData.description != "No more possible matches on board")
        {
            StartCoroutine(DisableInputOnGameOver());
        }
    }

    /// <summary>
    /// On game over event wait for the last move to finish, then disable input.
    /// </summary>
    /// <returns>Coroutine IEnumerator</returns>
    private IEnumerator DisableInputOnGameOver()
    {
        while (isClearRefillInProgress)
        {
            yield return null;
        }

        isInputEnabled = false;
    }

    private IEnumerator CheckIfMoveMade()
    {
        while (true)
        {
            yield return new WaitForSeconds(hintMarkerTimeout);

            if (!playerMoved)
            {

                new MoveHintStateChanged()
                {
                    isActive = true,
                    hintCoords = moveHint
                }.Fire();
            }

            playerMoved = false;
        }
    }
}