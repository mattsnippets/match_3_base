﻿using System;
using UnityEngine;

/// <summary>
/// Handles input from player.
/// </summary>
public class InputHandler
{
    private Tile selectedTile;
    private Tile targetTile;
    private Action<Tile, Tile> startSwitch;

    /// <summary>
    /// Ctor initializing selected and target tiles and callback method for switching logic.
    /// </summary>
    /// <param name="selectedTile">The tile where the player clicked first</param>
    /// <param name="targetTile">The tile which the piece has been dragged towards</param>
    /// <param name="startSwitch">Callback method to trigger a piece switch</param>
    public InputHandler(Tile selectedTile, Tile targetTile, Action<Tile, Tile> startSwitch)
    {
        this.selectedTile = selectedTile;
        this.targetTile = targetTile;
        this.startSwitch = startSwitch;
    }

    /// <summary>
    /// Make the clicked Tile object the selected tile.
    /// </summary>
    /// <param name="tile">The clicked tile</param>
    public void ClickTile(Tile tile)
    {
        if (selectedTile == null)
        {
            selectedTile = tile;
            CustomLogger.Log(string.Format("Click selected tile: {0} {1}", selectedTile.X, selectedTile.Y), LogPriority.Info);
        }
    }

    /// <summary>
    /// If the selected tile is not null and is being dragged on an adjacent tile, make the other
    /// tile the target tile.
    /// </summary>
    /// <param name="tile">The target of the drag movement</param>
    public void DragTile(Tile tile)
    {
        if (selectedTile != null && AreAdjacent(tile, selectedTile))
        {
            targetTile = tile;
            CustomLogger.Log(string.Format("Target tile: {0} {1}", tile.X, tile.Y), LogPriority.Info);
        }
    }

    /// <summary>
    /// If selected and target tiles are not null and selected tile is released, trigger swich logic
    /// and set selected and target to null.
    /// </summary>
    public void ReleaseTile()
    {
        if (selectedTile != null && targetTile != null)
        {
            CustomLogger.Log("ReleaseTile() tiles are not null, making switch", LogPriority.Info);
            startSwitch(selectedTile, targetTile);
        }

        selectedTile = null;
        targetTile = null;

        CustomLogger.Log("Releasing tiles", LogPriority.Info);
    }

    /// <summary>
    /// Check if two tiles are adjacent.
    /// </summary>
    /// <param name="a">First tile</param>
    /// <param name="b">Second tile</param>
    /// <returns>True if the tiles are adjacent</returns>
    private bool AreAdjacent(Tile a, Tile b)
    {
        if (Mathf.Abs(a.X - b.X) == 1 && a.Y == b.Y)
        {
            return true;
        }

        if (Mathf.Abs(a.Y - b.Y) == 1 && a.X == b.X)
        {
            return true;
        }

        return false;
    }
}
