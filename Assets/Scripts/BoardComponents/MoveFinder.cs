﻿using Mattsnippets.EventSystem;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// This class checks for possible moves on the board and returns a randomly picked one via FindMove().
/// </summary>
public class MoveFinder
{
    private int width;
    private int height;
    private PieceManager pieceManager;
    private MatchFinder matchFinder;
    private ArrayWrapper<Piece> piecesCopy;
    private List<CoordinatePair> possibleMoves;

    /// <summary>
    /// The ctor initializes board width and height fields, PieceManager and MatchFinder references.
    /// </summary>
    /// <param name="width">Board width</param>
    /// <param name="height">Board height</param>
    /// <param name="pieceManager">PieceManager reference</param>
    /// <param name="matchFinder">MatchFinder reference</param>
    public MoveFinder(int width, int height, PieceManager pieceManager, MatchFinder matchFinder)
    {
        this.width = width;
        this.height = height;
        this.pieceManager = pieceManager;
        this.matchFinder = matchFinder;
    }

    /// <summary>
    /// Makes a local copy of PieceManagers ArrayWrapper then tries to find moves by switching all
    /// the pieces with their upper and right neighbors to form a match. If no moves are found, a 
    /// game over event is broadcast. Otherwise a possible move is returned.
    /// </summary>
    /// <returns>A CoordinatePair struct. If no valid moves are found all of the return's coordinates
    /// are -1s</returns>
    public CoordinatePair FindMove()
    {
        piecesCopy = new ArrayWrapper<Piece>(pieceManager.PieceArrayWrapper);
        possibleMoves = new List<CoordinatePair>();

        for (int j = 0; j < height - 1; j++)
        {
            for (int i = 0; i < width - 1; i++)
            {
                if (piecesCopy[i, j] != null)
                {
                    if (piecesCopy[i, j].HasColorBomb)
                    {
                       CheckColorBombMoves(i, j);
                    }
                    else
                    {
                        SwitchAndCheckForMatches(j, i, 1, 0);
                        SwitchAndCheckForMatches(j, i, 0, 1);
                    }
                }
            }
        }

        if (!possibleMoves.Any())
        {
            new GameOver()
            {
                hasWon = false,
                heading = "No more matches",
                description = "No more possible matches on board"
            }.Fire();

            return new CoordinatePair(-1, -1, -1, -1);
        }

        return possibleMoves[Random.Range(0, possibleMoves.Count)];
    }

    /// <summary>
    /// Check all four neighbors of a color bomb for possible moves.
    /// </summary>
    /// <param name="i">First index of color bomb</param>
    /// <param name="j">Second index of color bomb</param>
    private void CheckColorBombMoves(int i, int j)
    {
        AddColorBombMove(i, j, i + 1, j);
        AddColorBombMove(i, j, i - 1, j);
        AddColorBombMove(i, j, i, j + 1);
        AddColorBombMove(i, j, i, j - 1);
    }

    /// <summary>
    /// If a color bomb's neigbor is within board and not null add it as a possible move.
    /// </summary>
    /// <param name="x">Bomb's x coordinate</param>
    /// <param name="y">Bomb's y coordinate</param>
    /// <param name="nX">Neighbor's x coordinate</param>
    /// <param name="nY">Neighbor's y coordinate</param>
    private void AddColorBombMove(int x, int y, int nX, int nY)
    {
        if (Board.IsWithinBoard(nX, nY) && piecesCopy[nX, nY] != null)
        {
            possibleMoves.Add(new CoordinatePair(x, y, nX, nY));
        }
    }

    /// <summary>
    /// Call switch in the given direction, invoke CheckMatches() then switch back to original place.
    /// </summary>
    /// <param name="j">First coordinate</param>
    /// <param name="i">Second coordinate</param>
    /// <param name="horizontalModifier">It is added to the horizontal position</param>
    /// <param name="verticalModifier">It is added to the vertical position</param>
    private void SwitchAndCheckForMatches(int j, int i, int horizontalModifier, int verticalModifier)
    {
        SwitchWithNeighbor(j, i, horizontalModifier, verticalModifier);
        CheckMatches(j, i, horizontalModifier, verticalModifier);
        SwitchWithNeighbor(j, i, horizontalModifier, verticalModifier);
    }

    /// <summary>
    /// Check if there are any matches formed after switching a piece with its upper or right neighbor.
    /// </summary>
    /// <param name="j">First coordinate</param>
    /// <param name="i">Second coordinate</param>
    /// <param name="horizontalModifier">It is added to the horizontal position</param>
    /// <param name="verticalModifier">It is added to the vertical position</param>
    private void CheckMatches(int j, int i, int horizontalModifier, int verticalModifier)
    {
        if (matchFinder.FindMatchesAt(i, j, piecesCopy).Any() || matchFinder.FindMatchesAt(
            i + horizontalModifier, j + verticalModifier, piecesCopy).Any())
        {
            possibleMoves.Add(new CoordinatePair(i, j, i + horizontalModifier,
                j + verticalModifier));
        }
    }

    /// <summary>
    /// Switch a piece with its upper or right neighbor if the neighbor is not null.
    /// </summary>
    /// <param name="j">First coordinate</param>
    /// <param name="i">Second coordinate</param>
    /// <param name="horizontalModifier">It is added to the horizontal position</param>
    /// <param name="verticalModifier">It is added to the vertical position</param>
    private void SwitchWithNeighbor(int j, int i, int horizontalModifier, int verticalModifier)
    {
        Piece tempPiece = piecesCopy[i + horizontalModifier, j + verticalModifier];

        if (tempPiece != null)
        {
            piecesCopy.SetElement(i + horizontalModifier, j + verticalModifier, piecesCopy[i, j]);
            piecesCopy.SetElement(i, j, tempPiece);

            piecesCopy[i, j].SetCoordinates(i, j);
            piecesCopy[i + horizontalModifier, j + verticalModifier].SetCoordinates(
                i + horizontalModifier, j + verticalModifier);
        }
    }
}
