﻿using Mattsnippets.EventSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

/// <summary>
/// Class serving as a data storage for game pieces and managing piece creation and removal.
/// </summary>
public class PieceManager : MonoBehaviour
{
    [SerializeField]
    private GameObject basicPiecePrefab;
    [SerializeField]
    private GameObject bombPrefab;
    [SerializeField]
    private StringIntDictionary scoreValues = StringIntDictionary.New<StringIntDictionary>();
    [SerializeField]
    private StringColorDictionary pieceColors = StringColorDictionary.New<StringColorDictionary>();

    private int width;
    private int height;
    private Piece[,] pieces;
    private ArrayWrapper<Piece> pieceArrayWrapper;
    private ParticleManager particleManager;
    private Piece[] hintMarkers;
    private AddScore addScore = new AddScore();

    public ArrayWrapper<Piece> PieceArrayWrapper
    {
        get
        {
            return pieceArrayWrapper;
        }
    }

    private void OnEnable()
    {
        MoveHintStateChanged.AddListener(OnMoveHintStateChanged);
    }

    private void OnDisable()
    {
        MoveHintStateChanged.RemoveListener(OnMoveHintStateChanged);
    }

    private void OnMoveHintStateChanged(MoveHintStateChanged eventData)
    {
        CoordinatePair coords = eventData.hintCoords;

        if (eventData.isActive)
        {
            hintMarkers = new Piece[]
            {
                pieceArrayWrapper[(int)coords.coord1.x, (int)coords.coord1.y],
                pieceArrayWrapper[(int)coords.coord2.x, (int)coords.coord2.y]
            };

            foreach (var marker in hintMarkers)
            {
                marker.GetComponent<Animator>().SetBool("IsMarkedAsHint", true);
                marker.transform.localPosition = new Vector3(marker.transform.localPosition.x,
                    marker.transform.localPosition.y, -5f);
                marker.gameObject.layer = LayerMask.NameToLayer("Highlight");
            }
        }
        else if (hintMarkers != null && hintMarkers.All(e => e != null))
        {
            foreach (var marker in hintMarkers)
            {
                marker.GetComponent<Animator>().SetBool("IsMarkedAsHint", false);
                marker.transform.localPosition = new Vector3(marker.transform.localPosition.x,
                    marker.transform.localPosition.y, 0f);
                marker.gameObject.layer = LayerMask.NameToLayer("Tiles");
            }
        }
    }

    /// <summary>
    /// Initialize board size fields and collections, set ParticleManager reference.
    /// </summary>
    /// <param name="width">Board width</param>
    /// <param name="height">Board height</param>
    public void Init(int width, int height)
    {
        Assert.raiseExceptions = true;
        this.width = width;
        this.height = height;
        pieces = new Piece[width, height];
        pieceArrayWrapper = new ArrayWrapper<Piece>(pieces);
        particleManager = gameObject.GetComponent<ParticleManager>();
    }

    /// <summary>
    /// Place a gamepiece by setting its transform position, coordinate fields and also creating its
    /// reference in the piece array.
    /// </summary>
    /// <param name="piece">The Piece to be placed</param>
    /// <param name="x">X coordinate</param>
    /// <param name="y">Y coordinate</param>
    public void PlacePiece(Piece piece, int x, int y)
    {
        Assert.IsNotNull(piece);

        piece.transform.position = new Vector3(x, y, 0f);
        piece.transform.rotation = Quaternion.identity;
        pieces[x, y] = piece;
        piece.SetCoordinates(x, y);
    }

    /// <summary>
    /// Create a new Piece object of the given prefab. Instantiate prefab, initialize its Piece script
    /// component, if it has a vertical offset then start to move it towards its destination.
    /// </summary>
    /// <param name="typeIndex">Index of the piece type to create</param>
    /// <param name="x">X coordinate</param>
    /// <param name="y">Y coordinate</param>
    /// <param name="verticalOffset">Vertical offset can be used to instantiate the piece above
    /// the play area to make it fall into place. Default value: 0</param>
    /// <param name="moveTime">The time it takes to move the piece to its destination. Default
    /// value: 0.1f</param>
    /// <returns>The created Piece</returns>
    public Piece CreatePiece(int typeIndex, int x, int y, int verticalOffset = 0,
        float moveTime = 0.1f)
    {
        GameObject createdPiece = ObjectPoolManager.Current.GetPooledObject(basicPiecePrefab);

        Assert.IsNotNull(createdPiece);

        Piece pieceObject = createdPiece.GetComponent<Piece>();

        pieceObject.Init((Piece piece, int xCoord, int yCoord) =>
        {
            PlacePiece(piece, xCoord, yCoord);
        }, scoreValues.dictionary[pieceObject.Type.ToString()], (PieceType)typeIndex);

        PlacePiece(pieceObject, x, y);

        createdPiece.transform.parent = transform;
        createdPiece.SetActive(true);

        if (verticalOffset != 0)
        {
            createdPiece.transform.position = new Vector3(x, y + verticalOffset, 0f);
            pieceObject.Move(x, y, moveTime);
        }

        return pieceObject;
    }

    /// <summary>
    /// Remove a gamepiece by setting its reference to null in the array and destroying the gameobject.
    /// Removal also triggers AddScore() and the creation of an explosion.
    /// </summary>
    /// <param name="x">X coordinate</param>
    /// <param name="y">Y coordinate</param>
    /// <param name="scoreMultiplier">Score multiplier used in AddScore(). Default value: 1</param>
    public void RemovePiece(int x, int y, int scoreMultiplier = 1)
    {
        Piece removedPiece = pieces[x, y];

        if (removedPiece != null)
        {
            AddScore(removedPiece, scoreMultiplier);
            pieces[x, y] = null;

            if (removedPiece.HasColorBomb)
            {
                particleManager.CreateExplosion(x, y, Color.white);
            }
            else
            {
                particleManager.CreateExplosion(x, y, pieceColors.dictionary[removedPiece.Type.ToString()]);
            }

            BombEffect bombEffect = removedPiece.GetComponentInChildren<BombEffect>();

            if (bombEffect != null)
            {
                bombEffect.Init();
            }

            foreach (Transform child in removedPiece.transform)
            {
                Destroy(child.gameObject);
            }

            removedPiece.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Overload of RemovePiece() which accepts a collection of pieces as input.
    /// </summary>
    /// <param name="pieces">Pieces to remove</param>
    /// <param name="scoreMultiplier">Score multiplier used in AddScore(). Default value: 1</param>
    public void RemovePiece(IEnumerable<Piece> pieces, int scoreMultiplier = 1)
    {
        foreach (Piece piece in pieces)
        {
            if (piece != null && piece.isActiveAndEnabled)
            {
                RemovePiece(piece.X, piece.Y, scoreMultiplier);
            }
        }
    }

    /// <summary>
    /// Calculate the score value of the piece then broadcast an AddScore event. If the piece has a
    /// bomb attached its value is also added to the piece's value and the whole sum is multiplied
    /// by the score multiplier.
    /// </summary>
    /// <param name="removedPiece">Reference to the removed Piece</param>
    /// <param name="scoreMultiplier">Final score is multiplied by this</param>
    private void AddScore(Piece removedPiece, int scoreMultiplier)
    {
        int bombScoreValue = 0;
        Bomb pieceBomb = removedPiece.GetComponentInChildren<Bomb>();

        if (pieceBomb != null)
        {
            bombScoreValue = pieceBomb.ScoreValue;
        }

        addScore.value = (removedPiece.ScoreValue + bombScoreValue) * scoreMultiplier;
        addScore.Fire();
    }

    /// <summary>
    /// Get a block of pieces in an offset * offset square.
    /// </summary>
    /// <param name="x">X center coordinate</param>
    /// <param name="y">Y center coordinate</param>
    /// <param name="offset">Offset of square</param>
    /// <returns>A List of pieces</returns>
    public List<Piece> GetPieceBlock(int x, int y, int offset)
    {
        List<Piece> blockPieces = new List<Piece>();

        for (int i = x - offset; i <= x + offset; i++)
        {
            for (int j = y - offset; j <= y + offset; j++)
            {
                if (Board.IsWithinBoard(i, j))
                {
                    blockPieces.Add(pieces[i, j]);
                }
            }
        }

        return blockPieces;
    }

    /// <summary>
    /// Get all the pieces in the given row.
    /// </summary>
    /// <param name="rowNum">Row number</param>
    /// <returns>A List of pieces</returns>
    public List<Piece> GetPieceRow(int rowNum)
    {
        List<Piece> rowPieces = new List<Piece>();

        for (int i = 0; i < width; i++)
        {
            if (pieces[i, rowNum] != null)
            {
                rowPieces.Add(pieces[i, rowNum]);
            }
        }

        return rowPieces;
    }

    /// <summary>
    /// Get all the pieces in the given column.
    /// </summary>
    /// <param name="columnNum">Column number</param>
    /// <returns>A List of pieces</returns>
    public List<Piece> GetPieceColumn(int columnNum)
    {
        List<Piece> columnPieces = new List<Piece>();

        for (int i = 0; i < height; i++)
        {
            if (pieces[columnNum, i] != null)
            {
                columnPieces.Add(pieces[columnNum, i]);
            }
        }

        return columnPieces;
    }

    /// <summary>
    /// Instantiate a bomb prefab, attach it to the piece at the given coordinates and initialize 
    /// the Bomb script.
    /// </summary>
    /// <param name="x">X coordinate of piece</param>
    /// <param name="y">Y coordinate of piece</param>
    /// <param name="bombType">Type of bomb to attach</param>
    public void AttachBomb(int x, int y, BombType bombType)
    {
        Assert.IsTrue(Board.IsWithinBoard(x, y));

        Transform parentPiece = pieces[x, y].gameObject.transform;
        GameObject attachedBomb = Instantiate(bombPrefab,
                            Vector3.zero, Quaternion.identity) as GameObject;
        attachedBomb.transform.parent = parentPiece;
        attachedBomb.transform.position = parentPiece.position;
        attachedBomb.GetComponent<Bomb>().Init(bombType, scoreValues.dictionary[bombType.ToString()]);

        if (bombType == BombType.Color)
        {
            parentPiece.GetComponent<SpriteRenderer>().sprite = null;
        }
    }

    /// <summary>
    /// Check if the input pieces form a corner match (T or L shape).
    /// </summary>
    /// <param name="pieces">A collection of pieces</param>
    /// <returns>True if the pieces form a corner match</returns>
    public bool IsCornerMatch(IEnumerable<Piece> pieces)
    {
        bool isVertical = false;
        bool isHorizontal = false;
        int startX = -1;
        int startY = -1;

        foreach (Piece piece in pieces)
        {
            if (piece != null)
            {
                if (startX == -1 || startY == -1)
                {
                    startX = piece.X;
                    startY = piece.Y;
                    continue;
                }

                if (piece.X != startX && piece.Y == startY)
                {
                    isHorizontal = true;
                }

                if (piece.X == startX && piece.Y != startY)
                {
                    isVertical = true;
                }
            }
        }

        return (isHorizontal && isVertical);
    }

    /// <summary>
    /// Create a BombData object based on the count of matching pieces and direction of switch.
    /// </summary>
    /// <param name="x">X coordinate of bomb</param>
    /// <param name="y">Y coordinate of bomb</param>
    /// <param name="switchDirection">Direction of switch</param>
    /// <param name="pieces">A collection of pieces in the match</param>
    /// <returns>A BombData object</returns>
    public BombData CreateBombData(int x, int y, Vector2 switchDirection, IEnumerable<Piece> pieces)
    {
        BombData bombData = null;

        if (pieces.Count() >= 4)
        {
            PieceType createdPieceType = pieces.First().Type;

            if (IsCornerMatch(pieces))
            {
                bombData = new BombData(x, y, BombType.Block, createdPieceType);
            }
            else
            {
                if (pieces.Count() >= 5)
                {
                    bombData = new BombData(x, y, BombType.Color, createdPieceType);
                }
                else
                {
                    if (switchDirection.y != 0)
                    {
                        bombData = new BombData(x, y, BombType.Row, createdPieceType);
                    }
                    else
                    {
                        bombData = new BombData(x, y, BombType.Column, createdPieceType);
                    }
                }
            }
        }

        return bombData;
    }

    /// <summary>
    /// Get the matches created by a color bomb. If both switched pieces are color bombs all pieces
    /// count as a match. Otherwise get all the pieces which has the same type as the non-color-bomb
    /// piece switched with the color bomb.
    /// </summary>
    /// <param name="selectedPiece">The selected Piece</param>
    /// <param name="targetPiece">The target Piece</param>
    /// <returns>A collection of matched pieces</returns>
    public IEnumerable<Piece> GetColorBombMatches(Piece selectedPiece, Piece targetPiece)
    {
        List<Piece> matches = new List<Piece>();

        if (selectedPiece.HasColorBomb && targetPiece.HasColorBomb)
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    matches.Add(pieces[i, j]);
                }
            }
        }
        else
        {
            CheckForColorBomb(selectedPiece, targetPiece, matches);
            CheckForColorBomb(targetPiece, selectedPiece, matches);
        }

        return matches;
    }

    /// <summary>
    /// A utility method handling the case of switching a color bomb with a non-color-bomb piece.
    /// </summary>
    /// <param name="checkedPiece">The piece to be checked for a color bomb</param>
    /// <param name="otherPiece">The other piece in the switch</param>
    /// <param name="matches">A List of matches</param>
    private void CheckForColorBomb(Piece checkedPiece, Piece otherPiece, List<Piece> matches)
    {
        if (checkedPiece.HasColorBomb)
        {
            matches.AddRange(GetPiecesOfType(otherPiece.Type));
            if (!matches.Contains(checkedPiece))
            {
                matches.Add(checkedPiece);
            }
        }
    }

    /// <summary>
    /// Get all pieces of the given type.
    /// </summary>
    /// <param name="pieceType">The piece type</param>
    /// <returns>A List of pieces of the given type</returns>
    private List<Piece> GetPiecesOfType(PieceType pieceType)
    {
        List<Piece> piecesOfType = new List<Piece>();

        foreach (Piece piece in pieces)
        {
            if (piece != null && !piece.HasColorBomb && piece.Type == pieceType)
            {
                piecesOfType.Add(piece);
            }
        }

        return piecesOfType;
    }
}
