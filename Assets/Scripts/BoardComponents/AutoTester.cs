﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Automatic tester class capable of playing the game automatically and providing interface for test
/// functionality.
/// </summary>
public class AutoTester
{
    private InputHandler inputHandler;
    private PieceManager pieceManager;
    private MoveFinder moveFinder;

    private ArrayWrapper<Tile> tiles;
    private int width;
    private int height;
    private int moves = 0;
    private Func<bool> checkBoardProgress;
    private Action<bool> setPieceRefill;

    /// <summary>
    /// Ctor initializing AutoTester fields.
    /// </summary>
    /// <param name="inputHandler">InputHandler reference</param>
    /// <param name="pieceManager">PieceManager reference</param>
    /// <param name="tiles">ArrayWrapper of tiles</param>
    /// <param name="width">Board width</param>
    /// <param name="height">Board height</param>
    /// <param name="checkBoardProgress">A callback for checking if the board content is being
    /// changed right now</param>
    /// <param name="setPieceRefill">A callback to turn refilling of board pieces on or off</param>
    /// <param name="moveFinder">MoveFinder reference</param>
    public AutoTester(InputHandler inputHandler, PieceManager pieceManager, Tile[,] tiles,
        int width, int height, Func<bool> checkBoardProgress, Action<bool> setPieceRefill,
        MoveFinder moveFinder)
    {
        this.inputHandler = inputHandler;
        this.pieceManager = pieceManager;
        this.tiles = new ArrayWrapper<Tile>(tiles);
        this.width = width;
        this.height = height;
        this.checkBoardProgress = checkBoardProgress;
        this.setPieceRefill = setPieceRefill;
        this.moveFinder = moveFinder;
    }

    public PieceManager PieceManager
    {
        get
        {
            return pieceManager;
        }
    }

    public bool IsMoveInProgress
    {
        get
        {
            return checkBoardProgress();
        }
    }

    /// <summary>
    /// Play the game in an infinite loop until there are no more valid moves available.
    /// </summary>
    /// <returns>Coroutine IEnumerator</returns>
    public IEnumerator TestMoveCoroutine()
    {
        CoordinatePair coordinates;

        while (true)
        {
            coordinates = moveFinder.FindMove();

            if (coordinates.coord1.x == -1 && coordinates.coord1.y == -1)
            {
                CustomLogger.Log(string.Format("AutoTester - no more matches possible. Moves: {0}", moves), LogPriority.Warning);
                break;
            }

            SimulatePieceSwitch(coordinates);

            while (checkBoardProgress())
            {
                yield return null;
            }

            moves++;
        }
    }

    /// <summary>
    /// Simulate a piece switch action: click on tile, dragging towards another tile, tile release.
    /// </summary>
    /// <param name="coordinates">A CoordinatePair of selected and target tile positions</param>
    public void SimulatePieceSwitch(CoordinatePair coordinates)
    {
        inputHandler.ClickTile(tiles[(int)coordinates.coord1.x, (int)coordinates.coord1.y]);
        inputHandler.DragTile(tiles[(int)coordinates.coord2.x, (int)coordinates.coord2.y]);
        inputHandler.ReleaseTile();
    }

    /// <summary>
    /// Reset all pieces (except on obstacle tiles) in a checker pattern.
    /// </summary>
    public void ResetPieces()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (tiles[i, j].Type != TileType.Obstacle)
                {
                    pieceManager.RemovePiece(i, j);

                    if ((i + j) % 2 == 0)
                    {
                        pieceManager.CreatePiece(0, i, j);
                    }
                    else
                    {
                        pieceManager.CreatePiece(1, i, j);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Remove all pieces from board.
    /// </summary>
    public void RemoveAllPieces()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (tiles[i, j].Type != TileType.Obstacle)
                {
                    pieceManager.RemovePiece(i, j);
                }
            }
        }
    }

    /// <summary>
    /// Remove current piece at coordinates and create a new instance of the given piece prefab.
    /// </summary>
    /// <param name="x">X coordinate</param>
    /// <param name="y">Y coordinate</param>
    /// <param name="piecePrefabIndex">Index of the Piece prefab to instantiate</param>
    public void SetPieceAt(int x, int y, int piecePrefabIndex)
    {
        pieceManager.RemovePiece(x, y);
        pieceManager.CreatePiece(piecePrefabIndex, x, y);
    }

    /// <summary>
    /// Call setPieceRefill() with the given bool value to turn piece refill on or off.
    /// </summary>
    /// <param name="isRefillEnabled">If true, pieces are refilled on the board after every move
    /// </param>
    public void SetPieceRefill(bool isRefillEnabled)
    {
        setPieceRefill(isRefillEnabled);
    }
}