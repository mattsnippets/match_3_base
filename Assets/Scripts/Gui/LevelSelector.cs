﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelector : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void OnLevelSelectClick(string level)
    {
        StartCoroutine(WaitForSceneLoad(level));
    }

   private IEnumerator WaitForSceneLoad(string sceneName)
    {
        var load = SceneManager.LoadSceneAsync(sceneName, (sceneName == "Settings" || sceneName == "Help")
            ? UnityEngine.SceneManagement.LoadSceneMode.Additive : UnityEngine.SceneManagement.LoadSceneMode.Single);

        while (!load.isDone)
        {
            yield return null;
        }

        SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
    }
}
