﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class AdditiveScreen : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UnloadScene();
        }
    }

    public void UnloadScene()
    {
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
    }
}
