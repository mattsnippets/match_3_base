﻿using Mattsnippets.EventSystem;
using System;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    [SerializeField]
    private Toggle musicToggle;
    [SerializeField]
    private Toggle flickerLightsToggle;

    private void Start()
    {
        musicToggle.isOn = Convert.ToBoolean(PlayerPrefs.GetInt("MusicEnabled", 1));
        flickerLightsToggle.isOn = Convert.ToBoolean(PlayerPrefs.GetInt("FlickerLightsEnabled", 1));
    }

    public void SetMusicEnabled(bool isEnabled)
    {
        PlayerPrefs.SetInt("MusicEnabled", Convert.ToInt32(isEnabled));
        new MusicEnabled() { isEnabled = isEnabled }.Fire();
    }

    public void SetFlickerLightsEnabled(bool isEnabled)
    {
        PlayerPrefs.SetInt("FlickerLightsEnabled", Convert.ToInt32(isEnabled));
    }
}
