﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(BoardIntegrationTestHelper))]
public class CheckColorBombDoesNotMatch : MonoBehaviour
{
    private BoardIntegrationTestHelper bith;

    private void Start()
    {
        bith = gameObject.GetComponent<BoardIntegrationTestHelper>();
        StartCoroutine(InitHelper());
    }

    private IEnumerator InitHelper()
    {
        while (!bith.IsInitialized)
        {
            yield return null;
        }

        bith.AutoTester.SetPieceRefill(false);
        yield return StartCoroutine(ColorBombSimpleMatch());
    }

    private IEnumerator ColorBombSimpleMatch()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 1, 5);
        bith.AutoTester.SetPieceAt(2, 0, 5);
        bith.AutoTester.SetPieceAt(1, 0, 4);

        bith.AutoTester.SetPieceAt(5, 5, 4);
        bith.AutoTester.SetPieceAt(6, 6, 4);
        bith.AutoTester.SetPieceAt(7, 5, 4);
        bith.AutoTester.SetPieceAt(6, 5, 5);

        bith.AutoTester.PieceManager.AttachBomb(0, 0, BombType.Color);
        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 1, 1, 0));

        yield return new WaitForSeconds(2f);

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        if (FindObjectsOfType<Bomb>().Length != 1)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                        "Some pieces were removed on color bomb simple 3 match.");
        }

        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 1, 5);
        bith.AutoTester.SetPieceAt(2, 0, 5);
        bith.AutoTester.SetPieceAt(3, 0, 5);
        bith.AutoTester.SetPieceAt(1, 0, 4);

        bith.AutoTester.SetPieceAt(5, 5, 4);
        bith.AutoTester.SetPieceAt(6, 6, 4);
        bith.AutoTester.SetPieceAt(7, 5, 4);
        bith.AutoTester.SetPieceAt(6, 5, 5);

        bith.AutoTester.PieceManager.AttachBomb(0, 0, BombType.Color);
        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 1, 1, 0));

        yield return new WaitForSeconds(2f);

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        if (FindObjectsOfType<Bomb>().Length != 1)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                        "Some pieces were removed on color bomb simple 4 match.");
        }

        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 0, 5);
        bith.AutoTester.SetPieceAt(2, 1, 5);
        bith.AutoTester.SetPieceAt(3, 0, 5);
        bith.AutoTester.SetPieceAt(4, 0, 5);
        bith.AutoTester.SetPieceAt(2, 0, 4);

        bith.AutoTester.SetPieceAt(5, 5, 4);
        bith.AutoTester.SetPieceAt(6, 6, 4);
        bith.AutoTester.SetPieceAt(7, 5, 4);
        bith.AutoTester.SetPieceAt(6, 5, 5);

        bith.AutoTester.PieceManager.AttachBomb(0, 0, BombType.Color);
        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(2, 1, 2, 0));

        yield return new WaitForSeconds(2f);

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        if (FindObjectsOfType<Bomb>().Length != 2)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                        "Some pieces were removed on color bomb simple 5 match.");
        }

        yield return StartCoroutine(ColorBombFallingBombMatch());
    }

    private IEnumerator ColorBombFallingBombMatch()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 0, 5);
        bith.AutoTester.SetPieceAt(2, 0, 4);
        bith.AutoTester.SetPieceAt(2, 1, 4);
        bith.AutoTester.SetPieceAt(2, 2, 3);
        bith.AutoTester.SetPieceAt(3, 2, 4);
        bith.AutoTester.SetPieceAt(3, 0, 5);
        bith.AutoTester.SetPieceAt(4, 0, 5);
        bith.AutoTester.SetPieceAt(2, 3, 5);

        bith.AutoTester.SetPieceAt(5, 5, 4);
        bith.AutoTester.SetPieceAt(6, 6, 4);
        bith.AutoTester.SetPieceAt(7, 5, 4);
        bith.AutoTester.SetPieceAt(6, 5, 5);

        bith.AutoTester.PieceManager.AttachBomb(2, 3, BombType.Color);
        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(2, 2, 3, 2));

        yield return new WaitForSeconds(2f);

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        if (FindObjectsOfType<Bomb>().Length != 1)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                        "Some pieces were removed on color bomb falling match.");
        }

        IntegrationTest.Pass();
    }
}
