﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(BoardIntegrationTestHelper))]
public class CheckDisableOnLose : MonoBehaviour
{
    private BoardIntegrationTestHelper bith;

    private void Start()
    {
        bith = gameObject.GetComponent<BoardIntegrationTestHelper>();
        StartCoroutine(InitHelper());
    }
    private IEnumerator InitHelper()
    {
        while (!bith.IsInitialized)
        {
            yield return null;
        }

        yield return StartCoroutine(DisableMoveOnLose());
    }

    private IEnumerator DisableMoveOnLose()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 1, 5);
        bith.AutoTester.SetPieceAt(2, 0, 5);
        bith.AutoTester.SetPieceAt(0, 1, 4);
        bith.AutoTester.SetPieceAt(2, 1, 4);
        bith.AutoTester.SetPieceAt(1, 0, 3);

        yield return new WaitForSeconds(2f);

        PieceType originalPieceType = bith.PieceArrayWrapper[0, 0].Type;

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 1, 1, 0));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        if (bith.PieceArrayWrapper[0, 0].Type != originalPieceType)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Pieces moved despite of disabled input upon game lose.");
        }

        IntegrationTest.Pass();
    }
}
