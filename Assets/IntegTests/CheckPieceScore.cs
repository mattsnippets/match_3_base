﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoardIntegrationTestHelper))]
public class CheckPieceScore : MonoBehaviour
{
    private BoardIntegrationTestHelper bith;
    private GameManager gameManager;
    StringIntDictionary scoreValues;
    Dictionary<string, int> scoreValuesDict;

    [SerializeField]
    private Text scoreText;

    private void Start()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();
        bith = gameObject.GetComponent<BoardIntegrationTestHelper>();

        scoreValues = gameObject.GetComponent<PieceManager>().GetType().
            GetField("scoreValues", BindingFlags.Instance | BindingFlags.NonPublic |
            BindingFlags.Public).GetValue(gameObject.GetComponent<PieceManager>()) as StringIntDictionary;

        scoreValuesDict = scoreValues.dictionary;

        StartCoroutine(InitHelper());
    }

    private IEnumerator InitHelper()
    {
        while (!bith.IsInitialized)
        {
            yield return null;
        }

        bith.AutoTester.SetPieceRefill(false);
        yield return StartCoroutine(SimpleSwitch());
    }

    private IEnumerator SimpleSwitch()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 1, 5);
        bith.AutoTester.SetPieceAt(2, 0, 5);
        bith.AutoTester.SetPieceAt(0, 1, 4);
        bith.AutoTester.SetPieceAt(2, 1, 4);
        bith.AutoTester.SetPieceAt(1, 0, 3);

        PieceType testPieceType = bith.PieceArrayWrapper[0, 0].Type;

        gameManager.ResetScore();

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 1, 1, 0));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        yield return new WaitForSeconds(3f);

        if (scoreText.text != (scoreValuesDict[testPieceType.ToString()] * 3).
            ToString())
        {
            IntegrationTest.Fail(transform.parent.gameObject, "Wrong score calculated for match 3.");
        }

        yield return StartCoroutine(RecursiveSwitch());
    }

    private IEnumerator RecursiveSwitch()
    {
        bith.AutoTester.RemoveAllPieces();

        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 1, 5);
        bith.AutoTester.SetPieceAt(2, 0, 5);
        bith.AutoTester.SetPieceAt(0, 1, 4);
        bith.AutoTester.SetPieceAt(2, 1, 4);
        bith.AutoTester.SetPieceAt(3, 1, 3);
        bith.AutoTester.SetPieceAt(2, 2, 3);
        bith.AutoTester.SetPieceAt(1, 2, 3);
        bith.AutoTester.SetPieceAt(1, 0, 3);
        bith.AutoTester.SetPieceAt(3, 0, 2);


        PieceType testPieceType1 = bith.PieceArrayWrapper[0, 0].Type;
        PieceType testPieceType2 = bith.PieceArrayWrapper[1, 2].Type;

        gameManager.ResetScore();

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 1, 1, 0));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        //yield return new WaitForSeconds(3f);

        if (scoreText.text != ((scoreValuesDict[testPieceType1.ToString()] * 3) +
          (scoreValuesDict[testPieceType2.ToString()] * 3 * 2)).
            ToString())
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong score calculated for recursive match 3.");
        }

        yield return StartCoroutine(BlockBombSwitch());
    }

    private IEnumerator BlockBombSwitch()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 1, 5);
        bith.AutoTester.SetPieceAt(1, 0, 5);
        bith.AutoTester.SetPieceAt(2, 1, 5);
        bith.AutoTester.SetPieceAt(0, 0, 4);
        bith.AutoTester.SetPieceAt(2, 0, 4);
        bith.AutoTester.SetPieceAt(1, 1, 3);
        bith.AutoTester.SetPieceAt(1, 2, 2);
        bith.AutoTester.SetPieceAt(0, 2, 1);
        bith.AutoTester.SetPieceAt(2, 2, 1);

        bith.AutoTester.PieceManager.AttachBomb(1, 0, BombType.Block);

        gameManager.ResetScore();

        int expectedScore = 0;

        for (int i = 0; i < bith.PieceArrayWrapper.GetLength(0); i++)
        {
            for (int j = 0; j < bith.PieceArrayWrapper.GetLength(1); j++)
            {
                if (bith.PieceArrayWrapper[i, j] != null)
                {
                    expectedScore += scoreValuesDict[bith.PieceArrayWrapper[i, j].Type.ToString()];
                }
            }
        }

        expectedScore += scoreValuesDict[BombType.Block.ToString()];

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 1, 1, 0));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        //yield return new WaitForSeconds(3f);


        if (scoreText.text != expectedScore.ToString())
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong score calculated for block bomb match.");
        }

        yield return StartCoroutine(RowBombSwitch());
    }

    private IEnumerator RowBombSwitch()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 0, 5);
        bith.AutoTester.SetPieceAt(2, 1, 5);
        bith.AutoTester.SetPieceAt(2, 0, 4);
        bith.AutoTester.SetPieceAt(3, 0, 1);
        bith.AutoTester.SetPieceAt(4, 0, 2);
        bith.AutoTester.SetPieceAt(5, 0, 1);
        bith.AutoTester.SetPieceAt(6, 0, 2);
        bith.AutoTester.SetPieceAt(7, 0, 1);

        bith.AutoTester.PieceManager.AttachBomb(2, 1, BombType.Row);

        gameManager.ResetScore();

        int expectedScore = 0;

        for (int i = 0; i < bith.PieceArrayWrapper.GetLength(0); i++)
        {
            for (int j = 0; j < bith.PieceArrayWrapper.GetLength(1); j++)
            {
                if (bith.PieceArrayWrapper[i, j] != null)
                {
                    expectedScore += scoreValuesDict[bith.PieceArrayWrapper[i, j].Type.ToString()];
                }
            }
        }

        expectedScore -= scoreValuesDict[bith.PieceArrayWrapper[2, 0].Type.ToString()];
        expectedScore += scoreValuesDict[BombType.Row.ToString()];

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(2, 1, 2, 0));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        //yield return new WaitForSeconds(3f);


        if (scoreText.text != expectedScore.ToString())
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong score calculated for row bomb match.");
        }

        yield return StartCoroutine(ColumnBombSwitch());
    }

    private IEnumerator ColumnBombSwitch()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(0, 1, 5);
        bith.AutoTester.SetPieceAt(1, 2, 5);
        bith.AutoTester.SetPieceAt(0, 2, 4);
        bith.AutoTester.SetPieceAt(0, 3, 1);
        bith.AutoTester.SetPieceAt(0, 4, 2);
        bith.AutoTester.SetPieceAt(0, 5, 1);
        bith.AutoTester.SetPieceAt(0, 6, 2);
        bith.AutoTester.SetPieceAt(0, 7, 1);

        bith.AutoTester.PieceManager.AttachBomb(1, 2, BombType.Column);

        gameManager.ResetScore();

        int expectedScore = 0;

        for (int i = 0; i < bith.PieceArrayWrapper.GetLength(0); i++)
        {
            for (int j = 0; j < bith.PieceArrayWrapper.GetLength(1); j++)
            {
                if (bith.PieceArrayWrapper[i, j] != null)
                {
                    expectedScore += scoreValuesDict[bith.PieceArrayWrapper[i, j].Type.ToString()];
                }
            }
        }

        expectedScore -= scoreValuesDict[bith.PieceArrayWrapper[0, 2].Type.ToString()];
        expectedScore += scoreValuesDict[BombType.Column.ToString()];

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 2, 0, 2));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        //yield return new WaitForSeconds(3f);


        if (scoreText.text != expectedScore.ToString())
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong score calculated for column bomb match.");
        }

        yield return StartCoroutine(ColorBombColorSwitch());
    }

    private IEnumerator ColorBombColorSwitch()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 0, 4);
        bith.AutoTester.SetPieceAt(2, 0, 1);
        bith.AutoTester.SetPieceAt(3, 0, 2);
        bith.AutoTester.SetPieceAt(0, 1, 4);
        bith.AutoTester.SetPieceAt(1, 1, 3);
        bith.AutoTester.SetPieceAt(2, 1, 4);

        bith.AutoTester.PieceManager.AttachBomb(0, 0, BombType.Color);

        gameManager.ResetScore();

        int expectedScore = 0;

        for (int i = 0; i < bith.PieceArrayWrapper.GetLength(0); i++)
        {
            for (int j = 0; j < bith.PieceArrayWrapper.GetLength(1); j++)
            {
                if (bith.PieceArrayWrapper[i, j] != null && bith.PieceArrayWrapper[i, j].Type ==
                    bith.PieceArrayWrapper[1, 0].Type)
                {
                    expectedScore += scoreValuesDict[bith.PieceArrayWrapper[i, j].Type.ToString()];
                }
            }
        }

        expectedScore += scoreValuesDict[BombType.Color.ToString()];
        expectedScore += scoreValuesDict[bith.PieceArrayWrapper[0, 0].Type.ToString()];

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(0, 0, 1, 0));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        yield return new WaitForSeconds(1f);


        if (scoreText.text != expectedScore.ToString())
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong score calculated for color bomb color switch.");
        }

        yield return StartCoroutine(ColorBombOtherColorBombSwitch());
    }

    private IEnumerator ColorBombOtherColorBombSwitch()
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 0, 4);
        bith.AutoTester.SetPieceAt(2, 0, 1);
        bith.AutoTester.SetPieceAt(3, 0, 2);
        bith.AutoTester.SetPieceAt(0, 1, 4);
        bith.AutoTester.SetPieceAt(1, 1, 3);
        bith.AutoTester.SetPieceAt(2, 1, 4);

        bith.AutoTester.PieceManager.AttachBomb(0, 0, BombType.Color);
        bith.AutoTester.PieceManager.AttachBomb(1, 0, BombType.Color);

        gameManager.ResetScore();

        int expectedScore = 0;

        for (int i = 0; i < bith.PieceArrayWrapper.GetLength(0); i++)
        {
            for (int j = 0; j < bith.PieceArrayWrapper.GetLength(1); j++)
            {
                if (bith.PieceArrayWrapper[i, j] != null)
                {
                    expectedScore += scoreValuesDict[bith.PieceArrayWrapper[i, j].Type.ToString()];
                }
            }
        }

        expectedScore += scoreValuesDict[BombType.Color.ToString()] * 2;

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(0, 0, 1, 0));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        yield return new WaitForSeconds(1f);


        if (scoreText.text != expectedScore.ToString())
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong score calculated for color bomb with other color bomb switch.");
        }

        IntegrationTest.Pass();
    }
}
