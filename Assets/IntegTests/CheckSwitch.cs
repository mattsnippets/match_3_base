﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(BoardIntegrationTestHelper))]
public class CheckSwitch : MonoBehaviour
{
    private BoardIntegrationTestHelper bith;

    private void Start()
    {
        bith = gameObject.GetComponent<BoardIntegrationTestHelper>();
        StartCoroutine(InitHelper());
    }

    private IEnumerator InitHelper()
    {
        while(!bith.IsInitialized)
        {
            yield return null;
        }

        yield return new WaitForSeconds(3f);

        yield return StartCoroutine(SimpleSwitch());
    }

    private IEnumerator SimpleSwitch()
    {       
        bith.AutoTester.ResetPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 1, 5);
        bith.AutoTester.SetPieceAt(2, 0, 5);
        bith.AutoTester.SetPieceAt(0, 1, 4);
        bith.AutoTester.SetPieceAt(2, 1, 4);

        int[] instanceIds = new int[3];

        instanceIds[0] = bith.PieceArrayWrapper[0, 0].GetInstanceID();
        instanceIds[1] = bith.PieceArrayWrapper[1, 1].GetInstanceID();
        instanceIds[2] = bith.PieceArrayWrapper[2, 0].GetInstanceID();

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 1, 1, 0));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        for (int i = 0; i < 3; i++)
        {
            if (bith.PieceArrayWrapper[i, 0].GetInstanceID() == instanceIds[i])
            {
                IntegrationTest.Fail(transform.parent.gameObject, "Some pieces weren't removed after a match.");
            }
        }

        for (int i = 0; i < bith.PieceArrayWrapper.GetLength(0); i++)
        {
            for (int j = 0; j < bith.PieceArrayWrapper.GetLength(1); j++)
            {
                if (bith.PieceArrayWrapper[i, j] == null)
                {
                    IntegrationTest.Fail(transform.parent.gameObject, "Unfilled pieces left after a match.");
                }
            }
        }

        yield return StartCoroutine(InvalidSwitch());
    }

    private IEnumerator InvalidSwitch()
    {
        bith.AutoTester.ResetPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 1, 3);
        bith.AutoTester.SetPieceAt(2, 0, 5);
        bith.AutoTester.SetPieceAt(0, 1, 4);
        bith.AutoTester.SetPieceAt(2, 1, 4);

        int[,] instanceIds = new int[3, 3];

        for (int i = 0; i < instanceIds.GetLength(0); i++)
        {
            for (int j = 0; j < instanceIds.GetLength(1); j++)
            {
                instanceIds[i, j] = bith.PieceArrayWrapper[i, j].GetInstanceID();
            }
        }

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 1, 1, 0));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        for (int i = 0; i < instanceIds.GetLength(0); i++)
        {
            for (int j = 0; j < instanceIds.GetLength(1); j++)
            {
                if (instanceIds[i, j] != bith.PieceArrayWrapper[i, j].GetInstanceID())
                {
                    IntegrationTest.Fail(transform.parent.gameObject,
                        "Some pieces changed during an invalid switch.");
                }
            }
        }

        yield return StartCoroutine(RecursiveSwitch());
    }

    private IEnumerator RecursiveSwitch()
    {
        bith.AutoTester.ResetPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 1, 5);
        bith.AutoTester.SetPieceAt(2, 0, 5);
        bith.AutoTester.SetPieceAt(0, 1, 4);
        bith.AutoTester.SetPieceAt(2, 1, 4);
        bith.AutoTester.SetPieceAt(3, 1, 3);
        bith.AutoTester.SetPieceAt(2, 2, 3);
        bith.AutoTester.SetPieceAt(1, 2, 3);

        int[] instanceIds = new int[3];

        instanceIds[0] = bith.PieceArrayWrapper[1, 2].GetInstanceID();
        instanceIds[1] = bith.PieceArrayWrapper[2, 2].GetInstanceID();
        instanceIds[2] = bith.PieceArrayWrapper[3, 1].GetInstanceID();

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 1, 1, 0));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        for (int i = 1; i < 4; i++)
        {
            if (bith.PieceArrayWrapper[i, 1].GetInstanceID() == instanceIds[i - 1])
            {
                IntegrationTest.Fail(transform.parent.gameObject, "Some pieces weren't removed after a match.");
            }
        }

        for (int i = 0; i < bith.PieceArrayWrapper.GetLength(0); i++)
        {
            for (int j = 0; j < bith.PieceArrayWrapper.GetLength(1); j++)
            {
                if (bith.PieceArrayWrapper[i, j] == null)
                {
                    IntegrationTest.Fail(transform.parent.gameObject, "Unfilled pieces left after a match.");
                }
            }
        }

        yield return StartCoroutine(MultiMatchSwitch());
    }

    private IEnumerator MultiMatchSwitch()
    {
        bith.AutoTester.ResetPieces();
        bith.AutoTester.SetPieceAt(1, 0, 5);
        bith.AutoTester.SetPieceAt(0, 1, 5);
        bith.AutoTester.SetPieceAt(2, 1, 5);
        bith.AutoTester.SetPieceAt(1, 2, 5);
        bith.AutoTester.SetPieceAt(1, 3, 5);

        int[] instanceIds = new int[5];

        instanceIds[0] = bith.PieceArrayWrapper[1, 0].GetInstanceID();
        instanceIds[1] = bith.PieceArrayWrapper[0, 1].GetInstanceID();
        instanceIds[2] = bith.PieceArrayWrapper[2, 1].GetInstanceID();
        instanceIds[3] = bith.PieceArrayWrapper[1, 2].GetInstanceID();
        instanceIds[4] = bith.PieceArrayWrapper[1, 3].GetInstanceID();

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(1, 0, 1, 1));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        for (int i = 0; i < bith.PieceArrayWrapper.GetLength(0); i++)
        {
            for (int j = 0; j < bith.PieceArrayWrapper.GetLength(1); j++)
            {
                if (bith.PieceArrayWrapper[i, j] == null)
                {
                    IntegrationTest.Fail(transform.parent.gameObject, "Unfilled pieces left after a match.");
                }
            }
        }

        bool isIdEqual = false;
        isIdEqual = IsInstanceIdEqual(instanceIds[0], 1, 0);
        isIdEqual = IsInstanceIdEqual(instanceIds[1], 0, 1);
        isIdEqual = IsInstanceIdEqual(instanceIds[2], 2, 1);
        isIdEqual = IsInstanceIdEqual(instanceIds[3], 1, 2);
        isIdEqual = IsInstanceIdEqual(instanceIds[4], 1, 3);

        if (isIdEqual)
        {
            IntegrationTest.Fail(transform.parent.gameObject, "Some pieces weren't removed after a match.");
        }

        IntegrationTest.Pass();
    }

    private bool IsInstanceIdEqual(int id, int x, int y)
    {
        return id == bith.PieceArrayWrapper[x, y].GetInstanceID();
    }
}