﻿using Mattsnippets.EventSystem;
using UnityEngine;

public class CheckScoreLimit : MonoBehaviour
{
    private bool added = false;

    void Update()
    {
        if (!added)
        {
            new AddScore() { value = 50 }.Fire();
            new AddScore() { value = 150 }.Fire();
            added = true;
        }
    }
}
