﻿using System.Collections;
using System.Reflection;
using UnityEngine;

[RequireComponent(typeof(BoardIntegrationTestHelper))]
public class CheckBombCreation : MonoBehaviour
{
    private BoardIntegrationTestHelper bith;

    private void Start()
    {
        bith = gameObject.GetComponent<BoardIntegrationTestHelper>();
        StartCoroutine(InitHelper());
    }

    private IEnumerator InitHelper()
    {
        while (!bith.IsInitialized)
        {
            yield return null;
        }

        yield return StartCoroutine(CreateBlockBomb());
    }


    private IEnumerator CreateBlockBomb()
    {
        yield return StartCoroutine(CreateBlockBombLogic(1, 0, 1, 1, true));
        yield return StartCoroutine(CreateBlockBombLogic(1, 1, 1, 0, false));
        yield return StartCoroutine(CreateColumnBomb());
    }

    private IEnumerator CreateBlockBombLogic(int x1, int y1, int x2, int y2, bool isMatchingPieceMoved)
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceRefill(false);
        bith.AutoTester.SetPieceAt(1, 0, 5);
        bith.AutoTester.SetPieceAt(0, 1, 5);
        bith.AutoTester.SetPieceAt(2, 1, 5);
        bith.AutoTester.SetPieceAt(1, 2, 5);
        bith.AutoTester.SetPieceAt(1, 3, 5);
        bith.AutoTester.SetPieceAt(0, 0, 2);
        bith.AutoTester.SetPieceAt(2, 0, 2);
        bith.AutoTester.SetPieceAt(1, 1, 1);

        PieceType matchType = bith.PieceArrayWrapper[1, 0].Type;

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(x1, y1, x2, y2));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        if (bith.PieceArrayWrapper[1, 1].GetComponentInChildren<Bomb>() == null)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "No block bomb attached to piece. " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }

        if (bith.PieceArrayWrapper[1, 1].GetComponentInChildren<Bomb>().BombType != BombType.Block)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong bomb type. Block bomb attached to piece " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }

        if (bith.PieceArrayWrapper[1, 1].GetComponentsInChildren<Bomb>().Length > 1)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Multiple bombs attached to piece. " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }

        if (bith.PieceArrayWrapper[1, 1].Type != matchType)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong bomb color. Block bomb attached to piece " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }
    }

    private IEnumerator CreateColumnBomb()
    {
        yield return StartCoroutine(CreateColumnBombLogic(0, 0, 1, 0, true));
        yield return StartCoroutine(CreateColumnBombLogic(1, 0, 0, 0, false));
        yield return StartCoroutine(CreateRowBomb());
    }

    private IEnumerator CreateColumnBombLogic(int x1, int y1, int x2, int y2, bool isMatchingPieceMoved)
    {
        bith.AutoTester.RemoveAllPieces();
        bith.AutoTester.SetPieceRefill(false);
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 1, 5);
        bith.AutoTester.SetPieceAt(1, 2, 5);
        bith.AutoTester.SetPieceAt(1, 3, 5);
        bith.AutoTester.SetPieceAt(1, 0, 1);

        PieceType matchType = bith.PieceArrayWrapper[0, 0].Type;

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(x1, y1, x2, y2));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        if (bith.PieceArrayWrapper[1, 0].GetComponentInChildren<Bomb>() == null)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "No column bomb attached to piece " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }

        if (bith.PieceArrayWrapper[1, 0].GetComponentInChildren<Bomb>().BombType != BombType.Column)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong bomb type. Column bomb attached to piece " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }

        if (bith.PieceArrayWrapper[1, 0].GetComponentsInChildren<Bomb>().Length > 1)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Multiple bombs attached to piece. " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }

        if (bith.PieceArrayWrapper[1, 0].Type != matchType)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong bomb color. Column bomb attached to piece " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }
    }

    private IEnumerator CreateRowBomb()
    {
        yield return StartCoroutine(CreateRowBombLogic(0, 0, 0, 1, true));
        yield return StartCoroutine(CreateRowBombLogic(0, 1, 0, 0, false));
        yield return StartCoroutine(CreateColorBomb());
    }

    private IEnumerator CreateRowBombLogic(int x1, int y1, int x2, int y2, bool isMatchingPieceMoved)
    {
        bith.AutoTester.SetPieceRefill(false);
        bith.AutoTester.ResetPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 1, 5);
        bith.AutoTester.SetPieceAt(2, 1, 5);
        bith.AutoTester.SetPieceAt(3, 1, 5);

        PieceType matchType = bith.PieceArrayWrapper[0, 0].Type;

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(x1, y1, x2, y2));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        if (bith.PieceArrayWrapper[0, 1].GetComponentInChildren<Bomb>() == null)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "No row bomb attached to piece " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }

        if (bith.PieceArrayWrapper[0, 1].GetComponentInChildren<Bomb>().BombType != BombType.Row)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong bomb type. Row bomb attached to piece " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }

        if (bith.PieceArrayWrapper[0, 1].GetComponentsInChildren<Bomb>().Length > 1)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Multiple bombs attached to piece. " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }

        if (bith.PieceArrayWrapper[0, 1].Type != matchType)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong bomb color. Row bomb attached to piece " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }
    }

    private IEnumerator CreateColorBomb()
    {
        yield return StartCoroutine(CreateColorBombLogic(0, 0, 1, 0, true));
        yield return StartCoroutine(CreateColorBombLogic(1, 0, 0, 0, false));
        IntegrationTest.Pass();
    }

    private IEnumerator CreateColorBombLogic(int x1, int y1, int x2, int y2, bool isMatchingPieceMoved)
    {
        bith.AutoTester.SetPieceRefill(false);
        bith.AutoTester.ResetPieces();
        bith.AutoTester.SetPieceAt(0, 0, 5);
        bith.AutoTester.SetPieceAt(1, 0, 1);
        bith.AutoTester.SetPieceAt(2, 0, 5);
        bith.AutoTester.SetPieceAt(3, 0, 5);
        bith.AutoTester.SetPieceAt(4, 0, 5);
        bith.AutoTester.SetPieceAt(5, 0, 5);

        PieceType matchType = bith.PieceArrayWrapper[0, 0].Type;

        bith.AutoTester.SimulatePieceSwitch(new CoordinatePair(x1, y1, x2, y2));

        while (bith.AutoTester.IsMoveInProgress)
        {
            yield return null;
        }

        if (bith.PieceArrayWrapper[1, 0].GetComponentInChildren<Bomb>() == null)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "No color bomb attached to piece " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }

        if (bith.PieceArrayWrapper[1, 0].GetComponentInChildren<Bomb>().BombType != BombType.Color)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong bomb type. Color bomb attached to piece " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }

        if (bith.PieceArrayWrapper[1, 0].GetComponentsInChildren<Bomb>().Length > 1)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Multiple bombs attached to piece. " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }

        if (bith.PieceArrayWrapper[1, 0].Type != matchType)
        {
            IntegrationTest.Fail(transform.parent.gameObject,
                "Wrong bomb color. Color bomb attached to piece " +
                BoardIntegrationTestHelper.MatchingPieceString(isMatchingPieceMoved));
        }
    }
}
