﻿using Mattsnippets.EventSystem;
using UnityEngine;

public class CheckMoveLimit : MonoBehaviour
{
    private bool moved = false;

    void Update()
    {
        if (!moved)
        {
            new ValidMove().Fire();
            new ValidMove().Fire();
            new ValidMove().Fire();

            moved = true;
        }
    }
}
