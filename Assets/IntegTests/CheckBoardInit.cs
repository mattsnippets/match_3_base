﻿using System.Reflection;
using UnityEngine;

public class CheckBoardInit : MonoBehaviour
{
    private Tile[] tiles;

    private void Update()
    {
        if (tiles == null && Time.time > 3f)
        {
            FieldInfo boardWidth = gameObject.GetComponent<Board>().GetType().GetField("width",
         BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
            FieldInfo boardHeight = gameObject.GetComponent<Board>().GetType().GetField("height",
         BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);

            tiles = gameObject.GetComponentsInChildren<Tile>();

            if (tiles.Length != (int)boardWidth.GetValue(gameObject) * (int)boardHeight.GetValue(gameObject))
            {
                IntegrationTest.Fail(transform.parent.gameObject,
                    "Not enough tiles in initialized board.");
            }

            Piece[] pieces = gameObject.GetComponentsInChildren<Piece>();

            if (pieces.Length != (int)boardWidth.GetValue(gameObject) * (int)boardHeight.GetValue(gameObject))
            {
                IntegrationTest.Fail(transform.parent.gameObject,
                    "Not enough pieces in initialized board.");
            }

            IntegrationTest.Pass();
        }
    }
}
