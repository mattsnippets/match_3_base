﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PieceManagerTest
{
    private int width = 8;
    private int height = 8;
    private PieceManager testPieceManager;
    private GameObject pieceContainerDummy;
    private GameObject pieceManagerHolder;
    private GameObject objectPoolManagerManagerHolder;
    private GameObject gui;
    private GameObject scoreText;
    private Dictionary<string, int> scoreValuesDict =
        new Dictionary<string, int> {
            { "Blue", 20 },
            { "Green", 20 },
            { "Grey", 20 },
            { "Purple", 20 },
            { "Red", 20 },
            { "Yellow", 20 },
            { "Block", 200 },
            { "Color", 500 },
            { "Column", 100 },
            { "Row", 100 }
        };
    private Dictionary<string, Color> pieceColorsDict =
        new Dictionary<string, Color> {
            { "Blue", Color.blue }
        };

    [SetUp]
    protected void SetUp()
    {
        pieceManagerHolder = new GameObject();
        pieceContainerDummy = new GameObject();
        objectPoolManagerManagerHolder = new GameObject();

        ParticleManager particleManager = pieceManagerHolder.AddComponent<ParticleManager>();

        GameObject particleSystemHolder = new GameObject();
        particleSystemHolder.AddComponent<ParticleSystem>();

        FieldInfo pieceExplosionPrefab = particleManager.GetType().
            GetField("pieceExplosionPrefab", BindingFlags.Instance | BindingFlags.NonPublic |
            BindingFlags.Public);

        pieceExplosionPrefab.SetValue(particleManager, particleSystemHolder);

        gui = new GameObject("Gui");
        scoreText = new GameObject("ScoreText");

        scoreText.transform.parent = gui.transform;
        scoreText.AddComponent<Text>();

        testPieceManager = pieceManagerHolder.AddComponent<PieceManager>();
        testPieceManager.Init(width, height);

        StringIntDictionary scoreValues = testPieceManager.GetType().
            GetField("scoreValues", BindingFlags.Instance | BindingFlags.NonPublic |
            BindingFlags.Public).GetValue(testPieceManager) as StringIntDictionary;

        scoreValues.dictionary = scoreValuesDict;

        StringColorDictionary pieceColors = testPieceManager.GetType().
            GetField("pieceColors", BindingFlags.Instance | BindingFlags.NonPublic |
            BindingFlags.Public).GetValue(testPieceManager) as StringColorDictionary;

        pieceColors.dictionary = pieceColorsDict;

        pieceManagerHolder.AddComponent<GameManager>();
        objectPoolManagerManagerHolder.AddComponent<ObjectPoolManager>();
    }

    [Test]
    public void InitTest()
    {
        Assert.AreEqual(width * height, testPieceManager.PieceArrayWrapper.Count);
    }

    [Test]
    public void PlacePieceTest()
    {
        GameObject pieceHolder = new GameObject();
        Piece testPiece = pieceHolder.AddComponent<Piece>();

        testPieceManager.PlacePiece(testPiece, 4, 5);

        Assert.AreEqual(testPiece, testPieceManager.PieceArrayWrapper[4, 5]);
        Assert.AreEqual(new Vector3(4f, 5f, 0f), testPiece.transform.position);
        Assert.AreEqual(Quaternion.identity, testPiece.transform.rotation);
        Assert.AreEqual(4, testPiece.X);
        Assert.AreEqual(5, testPiece.Y);
    }

    [Test]
    public void PlacePieceNullPieceTest()
    {
        Assert.Throws<UnityEngine.Assertions.AssertionException>(() => testPieceManager.PlacePiece(null, 4, 5));
    }

    [Test]
    public void CreatePieceTest()
    {
        GameObject testPieceObject = new GameObject();
        testPieceObject.AddComponent<Piece>();

        FieldInfo basicPiecePrefab = testPieceManager.GetType().GetField("basicPiecePrefab",
            BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        basicPiecePrefab.SetValue(testPieceManager, testPieceObject);

        Piece testCreatedPiece = testPieceManager.CreatePiece(0, 4, 5);

        Assert.IsNotNull(testCreatedPiece);
        Assert.AreEqual(pieceManagerHolder.transform, testCreatedPiece.transform.parent);
        Assert.AreEqual(testCreatedPiece, testPieceManager.PieceArrayWrapper[4, 5]);
        Assert.AreEqual(new Vector3(4f, 5f, 0f), testCreatedPiece.transform.position);
        Assert.AreEqual(Quaternion.identity, testCreatedPiece.transform.rotation);
        Assert.AreEqual(4, testCreatedPiece.X);
        Assert.AreEqual(5, testCreatedPiece.Y);
        Assert.AreEqual(20, testCreatedPiece.ScoreValue);
    }


    [Test]
    public void CreatePieceWithOffsetTest()
    {
        GameObject testPieceObject = new GameObject();
        testPieceObject.AddComponent<Piece>();

        FieldInfo basicPiecePrefab = testPieceManager.GetType().GetField("basicPiecePrefab",
            BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        basicPiecePrefab.SetValue(testPieceManager, testPieceObject);

        Piece testCreatedPiece = testPieceManager.CreatePiece(0, 4, 5, 1);

        Assert.IsNotNull(testCreatedPiece);
        Assert.AreEqual(pieceManagerHolder.transform, testCreatedPiece.transform.parent);
        Assert.AreEqual(testCreatedPiece, testPieceManager.PieceArrayWrapper[4, 5]);
        Assert.AreEqual(Quaternion.identity, testCreatedPiece.transform.rotation);
        Assert.AreEqual(4, testCreatedPiece.X);
        Assert.AreEqual(5, testCreatedPiece.Y);
        Assert.IsTrue(Vector3.Distance(testCreatedPiece.transform.position, new Vector3(4f, 5f, 0f)) < 0.5f);
    }

    [Test]
    public void RemovePieceSinglePieceTest()
    {
        GameObject pieceHolder = new GameObject("pieceHolder");
        Piece testPiece = pieceHolder.AddComponent<Piece>();
        testPieceManager.PlacePiece(testPiece, 4, 5);

        Assert.IsNotNull(pieceHolder);
        Assert.IsNotNull(testPieceManager.PieceArrayWrapper[4, 5]);

        testPieceManager.RemovePiece(4, 5);

        // TODO
        //Assert.IsNull(pieceHolder);
        Assert.IsNull(testPieceManager.PieceArrayWrapper[4, 5]);
    }

    [Test]
    public void RemovePieceSinglePieceNullTest()
    {
        Assert.DoesNotThrow(() => testPieceManager.RemovePiece(4, 5));
    }

    [Test]
    public void RemovePieceMultiPieceTest()
    {
        GameObject testPieceObject = new GameObject();
        testPieceObject.AddComponent<Piece>();

        FieldInfo basicPiecePrefab = testPieceManager.GetType().GetField("basicPiecePrefab",
            BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        basicPiecePrefab.SetValue(testPieceManager, testPieceObject);

        GameObject pieceHolder = new GameObject("pieceHolder");
        List<Piece> testPiecesToRemove = new List<Piece>();

        for (int i = 0; i < width; i++)
        {
            Piece testPiece = testPieceManager.CreatePiece(0, i, 0);
            testPiecesToRemove.Add(testPiece);
            Assert.IsNotNull(pieceHolder);
            Assert.IsNotNull(testPieceManager.PieceArrayWrapper[i, 0]);
        }

        testPieceManager.RemovePiece(testPiecesToRemove);

        for (int i = 0; i < width; i++)
        {
            Assert.IsNull(testPieceManager.PieceArrayWrapper[i, 0]);
        }
    }

    [Test]
    public void AttachBombTest()
    {
        // Initialize a bomb prefab for the test

        GameObject testBombPrefab = new GameObject();
        Bomb testBombObject = testBombPrefab.AddComponent<Bomb>();

        Sprite[] testBombSpritesArray = {
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero)
        };

        GameObject psHolder = new GameObject();
        ParticleSystem particleSystem = psHolder.AddComponent<ParticleSystem>();
        FieldInfo bombSparkleColor = testBombObject.GetType().GetField("bombSparkleColor",
            BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        bombSparkleColor.SetValue(testBombObject, particleSystem);

        FieldInfo bombPrefab = testPieceManager.GetType().GetField("bombPrefab",
         BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        bombPrefab.SetValue(testPieceManager, testBombPrefab);

        Sprite[] testSprites = new Sprite[]
        {
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero)
        };

        for (int i = 0; i < 3; i++)
        {

            GameObject pieceHolder = new GameObject("pieceHolder" + i);
            pieceHolder.AddComponent<SpriteRenderer>();
            Piece testPiece = pieceHolder.AddComponent<Piece>();
            testPieceManager.PlacePiece(testPiece, i, 0);
            Assert.IsNotNull(pieceHolder);
            Assert.IsNotNull(testPieceManager.PieceArrayWrapper[i, 0]);
            FieldInfo sprites = testPiece.GetType().GetField("sprites",
        BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            sprites.SetValue(testPiece, testSprites);

            testPieceManager.AttachBomb(i, 0, (BombType)i);
        }

        Bomb b1 = testPieceManager.PieceArrayWrapper[0, 0].GetComponentInChildren<Bomb>();
        Bomb b2 = testPieceManager.PieceArrayWrapper[1, 0].GetComponentInChildren<Bomb>();
        Bomb b3 = testPieceManager.PieceArrayWrapper[2, 0].GetComponentInChildren<Bomb>();

        for (int i = 0; i < 3; i++)
        {
            Bomb attachedBomb = testPieceManager.PieceArrayWrapper[i, 0].GetComponentInChildren<Bomb>();
            Assert.IsNotNull(attachedBomb);
            Assert.AreEqual(testPieceManager.PieceArrayWrapper[i, 0].transform.position,
                attachedBomb.transform.position);
            Assert.AreEqual((BombType)i, attachedBomb.BombType);
            Assert.AreEqual(scoreValuesDict[((BombType)i).ToString()], attachedBomb.ScoreValue);
        }
    }

    [Test]
    public void AttachBombOutOfBoundsTest()
    {
        Assert.Throws<UnityEngine.Assertions.AssertionException>(() =>
        testPieceManager.AttachBomb(20, 20, BombType.Block));
    }

    [Test]
    public void IsCornerMatchTest()
    {
        GameObject pieceHolder = new GameObject();
        List<Piece> testPieces1 = new List<Piece>();

        AddTestPiece(0, 0, pieceHolder, testPieces1);
        AddTestPiece(0, 1, pieceHolder, testPieces1);
        AddTestPiece(0, 2, pieceHolder, testPieces1);
        AddTestPiece(1, 0, pieceHolder, testPieces1);
        AddTestPiece(2, 0, pieceHolder, testPieces1);

        Assert.IsTrue(testPieceManager.IsCornerMatch(testPieces1));

        List<Piece> testPieces2 = new List<Piece>();

        // NOTE: the order of the added pieces matters in the test (emulate the real match find method)

        AddTestPiece(1, 0, pieceHolder, testPieces2);
        AddTestPiece(0, 0, pieceHolder, testPieces2);
        AddTestPiece(2, 0, pieceHolder, testPieces2);
        AddTestPiece(1, 1, pieceHolder, testPieces2);
        AddTestPiece(1, 2, pieceHolder, testPieces2);

        Assert.IsTrue(testPieceManager.IsCornerMatch(testPieces2));
    }

    private void AddTestPiece(int x, int y, GameObject pieceHolder, List<Piece> testPieces)
    {
        Piece testPiece = pieceHolder.AddComponent<Piece>();
        testPiece.SetCoordinates(x, y);
        testPieces.Add(testPiece);
    }

    [Test]
    public void CreateBombDataLessThan4Test()
    {
        GameObject pieceHolder = new GameObject();
        List<Piece> testPieces = new List<Piece>();
        AddTestPiece(0, 0, pieceHolder, testPieces);
        AddTestPiece(1, 0, pieceHolder, testPieces);
        AddTestPiece(2, 0, pieceHolder, testPieces);

        Assert.IsNull(testPieceManager.CreateBombData(0, 0, Vector2.zero, testPieces));
    }

    [Test]
    public void CreateBombDataCornerMatchTest()
    {
        GameObject pieceHolder = new GameObject();
        List<Piece> testPieces1 = new List<Piece>();

        AddTestPiece(0, 0, pieceHolder, testPieces1);
        AddTestPiece(0, 1, pieceHolder, testPieces1);
        AddTestPiece(0, 2, pieceHolder, testPieces1);
        AddTestPiece(1, 0, pieceHolder, testPieces1);
        AddTestPiece(2, 0, pieceHolder, testPieces1);

        BombData testBombData = testPieceManager.CreateBombData(0, 0, Vector2.zero, testPieces1);

        Assert.IsNotNull(testBombData);
        Assert.AreEqual(BombType.Block, testBombData.bombType);
        Assert.AreEqual(0, testBombData.x);
        Assert.AreEqual(0, testBombData.y);
        Assert.AreEqual(PieceType.Blue, testBombData.createdPieceType);
    }

    [Test]
    public void CreateBombDataNonCornerMatchRowTest()
    {
        GameObject pieceHolder = new GameObject();
        List<Piece> testPieces1 = new List<Piece>();

        AddTestPiece(0, 0, pieceHolder, testPieces1);
        AddTestPiece(0, 1, pieceHolder, testPieces1);
        AddTestPiece(0, 2, pieceHolder, testPieces1);
        AddTestPiece(0, 3, pieceHolder, testPieces1);

        BombData testBombData = testPieceManager.CreateBombData(0, 0, new Vector2(0, 1), testPieces1);

        Assert.IsNotNull(testBombData);
        Assert.AreEqual(BombType.Row, testBombData.bombType);
        Assert.AreEqual(0, testBombData.x);
        Assert.AreEqual(0, testBombData.y);
        Assert.AreEqual(PieceType.Blue, testBombData.createdPieceType);
    }


    [Test]
    public void CreateBombDataNonCornerMatchColumnTest()
    {
        GameObject pieceHolder = new GameObject();
        List<Piece> testPieces1 = new List<Piece>();

        AddTestPiece(0, 0, pieceHolder, testPieces1);
        AddTestPiece(1, 0, pieceHolder, testPieces1);
        AddTestPiece(2, 0, pieceHolder, testPieces1);
        AddTestPiece(3, 0, pieceHolder, testPieces1);

        BombData testBombData = testPieceManager.CreateBombData(0, 0, new Vector2(1, 0), testPieces1);

        Assert.IsNotNull(testBombData);
        Assert.AreEqual(BombType.Column, testBombData.bombType);
        Assert.AreEqual(0, testBombData.x);
        Assert.AreEqual(0, testBombData.y);
        Assert.AreEqual(PieceType.Blue, testBombData.createdPieceType);
    }

    [Test]
    public void CreateBombDataColorBombTest()
    {
        GameObject pieceHolder = new GameObject();
        List<Piece> testPieces = new List<Piece>();
        AddTestPiece(0, 0, pieceHolder, testPieces);
        AddTestPiece(1, 0, pieceHolder, testPieces);
        AddTestPiece(2, 0, pieceHolder, testPieces);
        AddTestPiece(3, 0, pieceHolder, testPieces);
        AddTestPiece(4, 0, pieceHolder, testPieces);

        BombData testBombData = testPieceManager.CreateBombData(0, 0, new Vector2(1, 0), testPieces);
        Assert.IsNotNull(testBombData);
        Assert.AreEqual(BombType.Color, testBombData.bombType);
        Assert.AreEqual(0, testBombData.x);
        Assert.AreEqual(0, testBombData.y);
        Assert.AreEqual(PieceType.Blue, testBombData.createdPieceType);
    }

    [Test]
    public void GetColorBombMatchesBothHaveBombsTest()
    {
        HashSet<Piece> testPieces = PrepareColorBombTests();

        IEnumerable<Piece> testMatches = testPieceManager.GetColorBombMatches(
            testPieceManager.PieceArrayWrapper[0, 0], testPieceManager.PieceArrayWrapper[1, 0]);

        Assert.IsTrue(testPieces.SetEquals(testMatches.Where(e => e != null)));
    }

    [Test]
    public void GetColorBombMatchesSelectedHasBombTest()
    {
        HashSet<Piece> testPieces = PrepareColorBombTests();

        IEnumerable<Piece> testMatches = testPieceManager.GetColorBombMatches(
            testPieceManager.PieceArrayWrapper[3, 2], testPieceManager.PieceArrayWrapper[2, 2]);

        HashSet<Piece> filteredTestPieces = new HashSet<Piece>(testPieces.
            Where(e => (e.Type == PieceType.Green && !e.HasColorBomb) || (e.X == 3 && e.Y == 2)));

        Assert.IsTrue(filteredTestPieces.SetEquals(testMatches.Where(e => e != null)));
    }

    [Test]
    public void GetColorBombMatchesTargetHasBombTest()
    {
        HashSet<Piece> testPieces = PrepareColorBombTests();

        IEnumerable<Piece> testMatches = testPieceManager.GetColorBombMatches(
            testPieceManager.PieceArrayWrapper[5, 5], testPieceManager.PieceArrayWrapper[6, 5]);

        HashSet<Piece> filteredTestPieces = new HashSet<Piece>(testPieces.
            Where(e => (e.Type == PieceType.Blue && !e.HasColorBomb) || (e.X == 6 && e.Y == 5)));

        Assert.IsTrue(filteredTestPieces.SetEquals(testMatches.Where(e => e != null)));
    }

    private HashSet<Piece> PrepareColorBombTests()
    {
        HashSet<Piece> testPieces = new HashSet<Piece>();

        GameObject testPieceObject = new GameObject();
        testPieceObject.AddComponent<SpriteRenderer>();
        Piece testPiece = testPieceObject.AddComponent<Piece>();
        testPiece.Type = PieceType.Blue;

        Sprite[] testSprites = new Sprite[]
        {
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero)
        };

        FieldInfo sprites = testPiece.GetType().GetField("sprites",
        BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        sprites.SetValue(testPiece, testSprites);

        FieldInfo basicPiecePrefab = testPieceManager.GetType().GetField("basicPiecePrefab",
            BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        basicPiecePrefab.SetValue(testPieceManager, testPieceObject);

        GameObject testBombPrefab = new GameObject();
        Bomb testBomb = testBombPrefab.AddComponent<Bomb>();
        testBombPrefab.AddComponent<SpriteRenderer>();

        Sprite[] bombPrefabSprites =
        {
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero)
        };

        FieldInfo bombPrefab = testPieceManager.GetType().GetField("bombPrefab",
     BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        bombPrefab.SetValue(testPieceManager, testBombPrefab);

        GameObject psHolder = new GameObject();
        ParticleSystem particleSystem = psHolder.AddComponent<ParticleSystem>();
        FieldInfo bombSparkleColor = testBomb.GetType().GetField("bombSparkleColor",
            BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        bombSparkleColor.SetValue(testBomb, particleSystem);

        testPieces.Add(testPieceManager.CreatePiece(0, 0, 0));
        testPieces.Add(testPieceManager.CreatePiece(0, 1, 0));
        testPieces.Add(testPieceManager.CreatePiece(1, 2, 2));
        testPieces.Add(testPieceManager.CreatePiece(1, 3, 2));
        testPieces.Add(testPieceManager.CreatePiece(0, 3, 3));
        testPieces.Add(testPieceManager.CreatePiece(1, 4, 4));
        testPieces.Add(testPieceManager.CreatePiece(0, 5, 5));
        testPieces.Add(testPieceManager.CreatePiece(1, 6, 5));
        testPieces.Add(testPieceManager.CreatePiece(1, 7, 7));

        testPieceManager.AttachBomb(0, 0, BombType.Color);
        testPieceManager.AttachBomb(1, 0, BombType.Color);
        testPieceManager.AttachBomb(3, 2, BombType.Color);
        testPieceManager.AttachBomb(6, 5, BombType.Color);

        return testPieces;
    }
}
