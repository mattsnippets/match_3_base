﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class MatchFinderTest
{
    private int width = 8;
    private int height = 8;
    private MatchFinder testMatchFinder;
    private Piece[,] testPieceArray;
    private GameObject pieceContainerDummy;

    [SetUp]
    protected void SetUp()
    {
        testMatchFinder = new MatchFinder(width, height);
        testPieceArray = new Piece[width, height];
        pieceContainerDummy = new GameObject();

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (i % 2 == 0)
                {
                    if (j % 2 == 0)
                    {
                        testPieceArray[i, j] = pieceContainerDummy.AddComponent<Piece>();
                        testPieceArray[i, j].Type = PieceType.Blue;
                        testPieceArray[i, j].SetCoordinates(i, j);
                    }
                    else
                    {
                        testPieceArray[i, j] = pieceContainerDummy.AddComponent<Piece>();
                        testPieceArray[i, j].Type = PieceType.Green;
                        testPieceArray[i, j].SetCoordinates(i, j);
                    }
                }
                else
                {
                    if (j % 2 == 0)
                    {
                        testPieceArray[i, j] = pieceContainerDummy.AddComponent<Piece>();
                        testPieceArray[i, j].Type = PieceType.Green;
                        testPieceArray[i, j].SetCoordinates(i, j);
                    }
                    else
                    {
                        testPieceArray[i, j] = pieceContainerDummy.AddComponent<Piece>();
                        testPieceArray[i, j].Type = PieceType.Blue;
                        testPieceArray[i, j].SetCoordinates(i, j);
                    }
                }
            }
        }
    }

    [Test]
    public void FindMatchesAtMatchLeftTest()
    {
        testPieceArray[0, 0].Type = PieceType.Red;
        testPieceArray[1, 0].Type = PieceType.Red;
        testPieceArray[2, 0].Type = PieceType.Red;

        IEnumerable<Piece> matches = testMatchFinder.FindMatchesAt(2, 0, new ArrayWrapper<Piece>
            (testPieceArray));

        Assert.AreEqual(3, matches.Count());
        Assert.IsTrue(matches.All(piece => piece.Type == PieceType.Red));
    }

    [Test]
    public void FindMatchesAtMatchRightTest()
    {
        testPieceArray[0, 0].Type = PieceType.Red;
        testPieceArray[1, 0].Type = PieceType.Red;
        testPieceArray[2, 0].Type = PieceType.Red;

        IEnumerable<Piece> matches = testMatchFinder.FindMatchesAt(0, 0, new ArrayWrapper<Piece>
            (testPieceArray));

        Assert.AreEqual(3, matches.Count());
        Assert.IsTrue(matches.All(piece => piece.Type == PieceType.Red));
    }

    [Test]
    public void FindMatchesAtMatchTopTest()
    {
        testPieceArray[0, 0].Type = PieceType.Red;
        testPieceArray[0, 1].Type = PieceType.Red;
        testPieceArray[0, 2].Type = PieceType.Red;

        IEnumerable<Piece> matches = testMatchFinder.FindMatchesAt(0, 0, new ArrayWrapper<Piece>
            (testPieceArray));

        Assert.AreEqual(3, matches.Count());
        Assert.IsTrue(matches.All(piece => piece.Type == PieceType.Red));
    }

    [Test]
    public void FindMatchesAtMatchBottomTest()
    {
        testPieceArray[0, 0].Type = PieceType.Red;
        testPieceArray[0, 1].Type = PieceType.Red;
        testPieceArray[0, 2].Type = PieceType.Red;

        IEnumerable<Piece> matches = testMatchFinder.FindMatchesAt(0, 2, new ArrayWrapper<Piece>
            (testPieceArray));

        Assert.AreEqual(3, matches.Count());
        Assert.IsTrue(matches.All(piece => piece.Type == PieceType.Red));
    }

    [Test]
    public void FindMatchesAtNoMatchTest()
    {
        IEnumerable<Piece> matches = testMatchFinder.FindMatchesAt(2, 0, new ArrayWrapper<Piece>
           (testPieceArray));

        Assert.IsTrue(!matches.Any());

        matches = testMatchFinder.FindMatchesAt(3, 3, new ArrayWrapper<Piece>
           (testPieceArray));

        Assert.IsTrue(!matches.Any());
    }

    [Test]
    public void FindMatchesAtColorBombInvalidMatchTest()
    {
        testPieceArray[0, 0].Type = PieceType.Red;
        testPieceArray[0, 1].Type = PieceType.Red;
        testPieceArray[0, 2].Type = PieceType.Red;

        GameObject testBombPrefab = new GameObject();
        Bomb testBomb = testBombPrefab.AddComponent<Bomb>();

        FieldInfo testBombType = testBomb.GetType().GetField("bombType",
           BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        testBombType.SetValue(testBomb, BombType.Color);

        Sprite[] bombPrefabSprites = { Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero),
            Sprite.Create(new Texture2D(1,1), new Rect(), Vector2.zero)
        };

        FieldInfo bombSprites = testBomb.GetType().GetField("sprites",
           BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
       // bombSprites.SetValue(testBomb, bombPrefabSprites);

        testBombPrefab.transform.parent = testPieceArray[0, 0].gameObject.transform;

        IEnumerable<Piece> matches = testMatchFinder.FindMatchesAt(0, 2, new ArrayWrapper<Piece>
            (testPieceArray));

        Assert.IsTrue(!matches.Any());
    }

    [Test]
    public void FindMatchesAtOutOfBoundsTest()
    {
        IEnumerable<Piece> matches = testMatchFinder.FindMatchesAt(10, 10, new ArrayWrapper<Piece>
           (testPieceArray));

        Assert.IsTrue(!matches.Any());
    }

    [Test]
    public void FindMatchesAt4MatchTest()
    {
        testPieceArray[0, 0].Type = PieceType.Red;
        testPieceArray[0, 1].Type = PieceType.Red;
        testPieceArray[0, 2].Type = PieceType.Red;
        testPieceArray[0, 3].Type = PieceType.Red;

        IEnumerable<Piece> matches = testMatchFinder.FindMatchesAt(0, 0, new ArrayWrapper<Piece>
            (testPieceArray));

        Assert.AreEqual(4, matches.Count());
        Assert.IsTrue(matches.All(piece => piece.Type == PieceType.Red));
    }

    [Test]
    public void FindMatchesAtTMatchTest()
    {
        testPieceArray[0, 0].Type = PieceType.Red;
        testPieceArray[1, 0].Type = PieceType.Red;
        testPieceArray[2, 0].Type = PieceType.Red;
        testPieceArray[3, 0].Type = PieceType.Red;
        testPieceArray[4, 0].Type = PieceType.Red;
        testPieceArray[2, 1].Type = PieceType.Red;
        testPieceArray[2, 2].Type = PieceType.Red;

        IEnumerable<Piece> matches = testMatchFinder.FindMatchesAt(2, 0, new ArrayWrapper<Piece>
            (testPieceArray));

        Assert.AreEqual(7, matches.Count());
        Assert.IsTrue(matches.All(piece => piece.Type == PieceType.Red));
    }

    [Test]
    public void FindMatchesAtLMatchTest()
    {
        testPieceArray[0, 0].Type = PieceType.Red;
        testPieceArray[1, 0].Type = PieceType.Red;
        testPieceArray[2, 0].Type = PieceType.Red;
        testPieceArray[3, 0].Type = PieceType.Red;
        testPieceArray[3, 1].Type = PieceType.Red;
        testPieceArray[3, 2].Type = PieceType.Red;
        testPieceArray[3, 3].Type = PieceType.Red;
        testPieceArray[3, 4].Type = PieceType.Red;

        IEnumerable<Piece> matches = testMatchFinder.FindMatchesAt(3, 0, new ArrayWrapper<Piece>
            (testPieceArray));

        Assert.AreEqual(8, matches.Count());
        Assert.IsTrue(matches.All(piece => piece.Type == PieceType.Red));
    }

    [Test]
    public void FindAllMatchesMatchTest()
    {
        testPieceArray[0, 0].Type = PieceType.Red;
        testPieceArray[1, 0].Type = PieceType.Red;
        testPieceArray[2, 0].Type = PieceType.Red;
        testPieceArray[3, 0].Type = PieceType.Red;
        testPieceArray[3, 1].Type = PieceType.Red;
        testPieceArray[3, 2].Type = PieceType.Red;
        testPieceArray[3, 3].Type = PieceType.Red;
        testPieceArray[3, 4].Type = PieceType.Red;
        testPieceArray[0, 7].Type = PieceType.Red;
        testPieceArray[1, 7].Type = PieceType.Red;
        testPieceArray[2, 7].Type = PieceType.Red;


        IEnumerable<Piece> matches = testMatchFinder.FindAllMatches(new ArrayWrapper<Piece>
            (testPieceArray));

        Assert.AreEqual(11, matches.Count());
        Assert.IsTrue(matches.All(piece => piece.Type == PieceType.Red));
    }

    [Test]
    public void FindAllMatchesMulticolorMatchTest()
    {
        testPieceArray[0, 0].Type = PieceType.Red;
        testPieceArray[1, 0].Type = PieceType.Red;
        testPieceArray[2, 0].Type = PieceType.Red;
        testPieceArray[3, 0].Type = PieceType.Red;
        testPieceArray[3, 1].Type = PieceType.Red;
        testPieceArray[3, 2].Type = PieceType.Red;
        testPieceArray[3, 3].Type = PieceType.Red;
        testPieceArray[3, 4].Type = PieceType.Red;
        testPieceArray[0, 7].Type = PieceType.Yellow;
        testPieceArray[1, 7].Type = PieceType.Yellow;
        testPieceArray[2, 7].Type = PieceType.Yellow;


        IEnumerable<Piece> matches = testMatchFinder.FindAllMatches(new ArrayWrapper<Piece>
            (testPieceArray));

        Assert.AreEqual(11, matches.Count());
        Assert.AreEqual(8, matches.Count(piece => piece.Type == PieceType.Red));
        Assert.AreEqual(3, matches.Count(piece => piece.Type == PieceType.Yellow));
    }

    [Test]
    public void FindAllMatchesNoMatchTest()
    {
        IEnumerable<Piece> matches = testMatchFinder.FindAllMatches(new ArrayWrapper<Piece>
           (testPieceArray));
        Assert.IsTrue(!matches.Any());
    }
}
